package com.example.unown.sellanytime.pojos;

public class ItemListMain {

    private String title,price,location,description,image,category,user,user_name,user_email,user_image,full_detail;
    private int id;

    public ItemListMain(String title, String price, String location, String description, String image, String category, String user, String user_name, String user_email, String user_image, int id,String full_detail) {
        this.title = title;
        this.price = price;
        this.location = location;
        this.description = description;
        this.image = image;
        this.category = category;
        this.user = user;
        this.user_name = user_name;
        this.user_email = user_email;
        this.user_image = user_image;
        this.id = id;
        this.full_detail = full_detail;
    }

    public String getFull_detail() {
        return full_detail;
    }

    public void setFull_detail(String full_detail) {
        this.full_detail = full_detail;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getUser_email() {
        return user_email;
    }

    public void setUser_email(String user_email) {
        this.user_email = user_email;
    }

    public String getUser_image() {
        return user_image;
    }

    public void setUser_image(String user_image) {
        this.user_image = user_image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
