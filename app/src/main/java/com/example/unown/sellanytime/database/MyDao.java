package com.example.unown.sellanytime.database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

/**
 * Created by unown on 8/6/2018.
 */

@Dao
public interface MyDao {

    @Insert
    public void insertDetail(User user);

    @Query("select * from users")
    public List<User> getUser();

    @Delete
    public void deleteUser(User user);


}
