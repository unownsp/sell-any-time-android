package com.example.unown.sellanytime.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.unown.sellanytime.R;


public class IntroFragment extends Fragment {

    private static final String ARG_PARAM1 = "id";
    int id;

    public IntroFragment() {
        // Required empty public constructor
    }

    public static IntroFragment newInstance(int param1) {
        IntroFragment fragment = new IntroFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_PARAM1, param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            id = getArguments().getInt(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_intro_sell, container, false);
        switch (id) {
            case 0:
                view = inflater.inflate(R.layout.layout_intro_sell, container, false);
                break;
            case 1:
                view = inflater.inflate(R.layout.layout_intro_buy, container, false);
                break;
            case 2:
                view = inflater.inflate(R.layout.layout_intro_donate, container, false);
                break;
        }
        return view;
    }

}
