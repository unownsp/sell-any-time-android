package com.example.unown.sellanytime.activities.user;

import android.arch.persistence.room.Room;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.unown.sellanytime.R;
import com.example.unown.sellanytime.adapters.MyItemListAdapter;
import com.example.unown.sellanytime.database.AppDatabase;
import com.example.unown.sellanytime.database.User;
import com.example.unown.sellanytime.network.APICreator;
import com.example.unown.sellanytime.pojos.ItemListMain;
import com.example.unown.sellanytime.utilities.MySingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MyItemsActivity extends AppCompatActivity {

    AppDatabase appDatabase;
    String access_token, TAG = "MyItemsActivity";
    ArrayList<ItemListMain> list = new ArrayList<>();

    RecyclerView recyclerView;
    RecyclerView.LayoutManager layoutManager;
    RecyclerView.Adapter adapter;

   public static MyItemsActivity myItemsActivity;
   boolean taskPerforned = false;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_items);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        init();

        getItems();
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (taskPerforned){
            getItems();
            taskPerforned = false;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void init() {

        appDatabase = Room.databaseBuilder(getApplicationContext(),
                AppDatabase.class,
                "userdb")
                .fallbackToDestructiveMigration()
                .allowMainThreadQueries()
                .build();
        List<User> user = appDatabase.myDao().getUser();

        for (User usr : user) {
            access_token = usr.getAccess_token();

        }

        myItemsActivity = this;
    }

    private void getItems() {

        StringRequest stringRequest = new StringRequest(Request.Method.GET, APICreator.MY_ITEMS(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.d(TAG, "onResponse: " + response);

                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = jsonObject.getJSONArray("data");

                            for (int i = 0; i < jsonArray.length(); i++) {

                                JSONObject JO = jsonArray.getJSONObject(i);

                                Log.d(TAG, "onResponse: " + JO.toString());

                                String full_detail = JO.toString();

                                String name = JO.getString("name");
                                int id = JO.getInt("id");
                                String description = JO.getString("description");
                                String image = JO.getString("image");
                                String avaliable_location = JO.getString("avaliable_location");
                                String category = JO.getString("category");
                                String price = JO.getString("price");
                                String user =  "jkg";

                                //JSONObject user_detail = new JSONObject(user);
                                String user_name = "jkg";
                                String user_email = "jkg";
                                String user_image = "jkg";

                                ItemListMain itemListMain = new ItemListMain(name, price, avaliable_location, description, image, category, user, user_name, user_email, user_image, id, full_detail);
                                list.add(itemListMain);
                            }

                            recyclerView = findViewById(R.id.MyItemsRecycler);
                            recyclerView.setHasFixedSize(true);

                            layoutManager = new LinearLayoutManager(getApplicationContext());
                            recyclerView.setLayoutManager(layoutManager);

                            adapter = new MyItemListAdapter(list, MyItemsActivity.this);
                            recyclerView.setAdapter(adapter);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.d(TAG, "onResponse: " + error);

            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Accept", "application/json");
                headers.put("Authorization", "Bearer " + access_token);

                return headers;
            }
        };

        MySingleton.getInstance(getApplicationContext()).addToRequestque(stringRequest);


    }
}
