package com.example.unown.sellanytime.database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

@Dao
public interface CartItemDao {

    @Insert
     void insertItem(CartItemPOJO cartItemPOJO);

    @Query("select * from cartItem")
     List<CartItemPOJO> getCartItem();

    @Delete
     void deleteItem(CartItemPOJO cartItemPOJO);

    @Query("DELETE from cartItem")
    void clearCart();
}
