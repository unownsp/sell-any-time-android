package com.example.unown.sellanytime.adapters;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.unown.sellanytime.R;
import com.example.unown.sellanytime.activities.user.ItemDetailActivity;
import com.example.unown.sellanytime.network.APICreator;
import com.example.unown.sellanytime.pojos.ItemListMain;

import java.util.ArrayList;

public class ItemListAdapter extends RecyclerView.Adapter<ItemListAdapter.ItemViewHolder> {


    private ArrayList<ItemListMain> itemLists = new ArrayList<>();
    private Activity mContext;

    public ItemListAdapter(ArrayList<ItemListMain> itemLists, Activity mContext) {

        this.itemLists = itemLists;
        this.mContext = mContext;
    }


    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_item_main, parent, false);
        return new ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {

        final ItemListMain itemListMain = itemLists.get(position);
        holder.title.setText(itemListMain.getTitle());
        holder.category.setText(itemListMain.getCategory());
        holder.price.setText(itemListMain.getPrice());
        holder.description.setText(itemListMain.getDescription());
        holder.location.setText(itemListMain.getLocation());
        holder.userDetail.setText(itemListMain.getUser());
        holder.id.setText(itemListMain.getId() + "");

        holder.user_name.setText(itemListMain.getUser_name());
        holder.user_email.setText(itemListMain.getUser_email());

          /* Glide.with(mContext)
                    .load(APICreator.ITEM_IMAGE()+itemList.getImage())
                    .into(holder.item_image);*/

        Glide.with(mContext)
                .load(APICreator.ITEM_IMAGE() + itemListMain.getImage())
                .into(holder.item_image);

        Glide.with(mContext)
                .load(APICreator.PROFILE_IMAGE() + itemListMain.getUser_image())
                .into(holder.user_image);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(mContext, ItemDetailActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("detail", itemListMain.getFull_detail());
                intent.putExtras(bundle);
                mContext.startActivity(intent);

            }
        });


    }

    @Override
    public int getItemCount() {
        return itemLists.size();
    }

    public static class ItemViewHolder extends RecyclerView.ViewHolder {

        TextView title, category, price, description, location, userDetail, id, user_name, user_email;
        ImageView item_image, user_image;

        public ItemViewHolder(View itemView) {
            super(itemView);

            title = itemView.findViewById(R.id.item_itemName);
            category = itemView.findViewById(R.id.item_itemCategory);
            price = itemView.findViewById(R.id.item_itemPrice);
            description = itemView.findViewById(R.id.item_itemDescription);
            location = itemView.findViewById(R.id.item_itemLocation);
            userDetail = itemView.findViewById(R.id.item_userDetail);
            id = itemView.findViewById(R.id.item_itemId);

            user_name = itemView.findViewById(R.id.item_userName);
            user_email = itemView.findViewById(R.id.item_userEmail);

            item_image = itemView.findViewById(R.id.item_itemImage);
            user_image = itemView.findViewById(R.id.item_userImage);


        }
    }

}