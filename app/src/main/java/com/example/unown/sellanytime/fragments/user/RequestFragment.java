package com.example.unown.sellanytime.fragments.user;


import android.arch.persistence.room.Room;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.unown.sellanytime.R;
import com.example.unown.sellanytime.adapters.RequestsAdapter;
import com.example.unown.sellanytime.database.AppDatabase;
import com.example.unown.sellanytime.database.User;
import com.example.unown.sellanytime.network.APICreator;
import com.example.unown.sellanytime.pojos.RequestList;
import com.example.unown.sellanytime.utilities.MySingleton;
import com.example.unown.sellanytime.utilities.SharedPref;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RequestFragment extends Fragment {

    View view;
    RecyclerView recyclerView;
    RecyclerView.LayoutManager layoutManager;
    RecyclerView.Adapter adapter;
    ArrayList<RequestList> requestLists = new ArrayList<>();
    AppDatabase appDatabase;
    String access_token,TAG = "RequestFragment",userid;

    public RequestFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_request, container, false);
        init();
        getData();
        return view;
    }

    private void init(){
        appDatabase = Room.databaseBuilder(getContext(),
                AppDatabase.class,
                "userdb")
                .fallbackToDestructiveMigration()
                .allowMainThreadQueries()
                .build();
        List<User> user = appDatabase.myDao().getUser();
        for (User usr : user) {
            access_token = usr.getAccess_token();
        }
        SharedPref.init(getContext());
        userid = SharedPref.read(SharedPref.UID, "-1");
    }

    private void getData(){
        StringRequest stringRequest = new StringRequest(Request.Method.GET, APICreator.REQUESTS(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d(TAG, "onResponse: "+response);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = jsonObject.getJSONArray("data");
                            for (int i=0;i<jsonArray.length();i++){
                                JSONObject JO = jsonArray.getJSONObject(i);
                                String name = JO.getString("name");
                                String description = JO.getString("description");
                                String location = JO.getString("available_location");
                                String price = JO.getString("price");
                                String category = JO.getString("category");
                                int id = JO.getInt("id");
                                int user_id = JO.getInt("user_id");
                                if (userid.equals("" + user_id)) {
                                    continue;
                                }
                                RequestList requestList = new RequestList();
                                requestList.setAvailable_location(location);
                                requestList.setDescription(description);
                                requestList.setId(id);
                                requestList.setPrice(price);
                                requestList.setName(name);
                                requestList.setCategory(category);
                                requestList.setUser_id(user_id);
                                requestLists.add(requestList);
                            }
                            recyclerView = view.findViewById(R.id.requestRecycler);
                            recyclerView.setHasFixedSize(true);

                            layoutManager = new LinearLayoutManager(getContext());
                            recyclerView.setLayoutManager(layoutManager);

                            adapter = new RequestsAdapter(requestLists,getActivity());
                            recyclerView.setAdapter(adapter);


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.d(TAG, "onErrorResponse: "+error);

            }
        }){

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Accept", "application/json");
                headers.put("Authorization", "Bearer " + access_token);
                return headers;
            }
        };

        MySingleton.getInstance(getContext()).addToRequestque(stringRequest);
    }

}
