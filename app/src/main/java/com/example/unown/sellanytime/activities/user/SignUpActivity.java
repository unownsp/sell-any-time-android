package com.example.unown.sellanytime.activities.user;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.unown.sellanytime.R;
import com.example.unown.sellanytime.customUI.ShowToast;
import com.example.unown.sellanytime.customUI.UIHelper;
import com.example.unown.sellanytime.network.APICreator;
import com.example.unown.sellanytime.utilities.MySingleton;
import com.example.unown.sellanytime.utilities.SharedPref;
import com.example.unown.sellanytime.utilities.Validator;

import java.util.HashMap;
import java.util.Map;

public class SignUpActivity extends AppCompatActivity {

    EditText first_name, last_name, address, email, password, phone, cpassword;
    ImageView indicatorEmail, indicatorPhone, indicatorFirstName, indicatorLastName, indicatorAddress, indicatorPassword, indicatorCPassword;
    Button signUpButton;
    String TAG = "SignUpActivity";

    SharedPreferences sharedPreferences;

    String firstName, lastName, location, eMail, phoneNumber, passWord, cPassword,fToken;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        init();
        listeners();
    }

    public void init() {

        sharedPreferences=getApplicationContext().getSharedPreferences(getString(R.string.fcm_key),MODE_PRIVATE);
        fToken = sharedPreferences.getString(getString(R.string.firebase_token),null);

        Toast.makeText(getApplicationContext(),fToken,Toast.LENGTH_SHORT).show();

        first_name = findViewById(R.id.first_nameET);
        last_name = findViewById(R.id.last_nameET);
        address = findViewById(R.id.addressET);
        email = findViewById(R.id.emailET);
        password = findViewById(R.id.passwordET);
        phone = findViewById(R.id.phoneET);
        cpassword = findViewById(R.id.CpasswordET);

        indicatorEmail = findViewById(R.id.indicatorEmail);
        indicatorPhone = findViewById(R.id.indicatorPhone);
        indicatorFirstName = findViewById(R.id.indicatorFirstName);
        indicatorLastName = findViewById(R.id.indicatorLastName);
        indicatorAddress = findViewById(R.id.indicatorAddress);
        indicatorPassword = findViewById(R.id.indicatorPass);
        indicatorCPassword = findViewById(R.id.indicatorCPass);

        signUpButton = findViewById(R.id.signUpButton);

        cpassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                if (Validator.isValidPassword(charSequence)) {
                    UIHelper.validationSucessTint(SignUpActivity.this, indicatorCPassword);
                } else {
                    UIHelper.validationNormalTint(SignUpActivity.this, indicatorCPassword);
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        password.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (Validator.isValidPassword(charSequence)) {
                    UIHelper.validationSucessTint(SignUpActivity.this, indicatorPassword);
                } else {
                    UIHelper.validationNormalTint(SignUpActivity.this, indicatorPassword);
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        address.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                if (!charSequence.equals("")) {
                    UIHelper.validationSucessTint(SignUpActivity.this, indicatorAddress);
                } else {
                    UIHelper.validationNormalTint(SignUpActivity.this, indicatorAddress);
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        email.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                if (Validator.isValidEmail(charSequence)) {
                    UIHelper.validationSucessTint(SignUpActivity.this, indicatorEmail);
                } else {
                    UIHelper.validationNormalTint(SignUpActivity.this, indicatorEmail);
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        phone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                if (Validator.isValidPhone(charSequence)) {
                    UIHelper.validationSucessTint(SignUpActivity.this, indicatorPhone);
                } else {
                    UIHelper.validationNormalTint(SignUpActivity.this, indicatorPhone);
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        first_name.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                if (!charSequence.equals("")) {
                    UIHelper.validationSucessTint(SignUpActivity.this, indicatorFirstName);
                } else {
                    UIHelper.validationNormalTint(SignUpActivity.this, indicatorFirstName);
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        last_name.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                if (!charSequence.equals("")) {
                    UIHelper.validationSucessTint(SignUpActivity.this, indicatorLastName);
                } else {
                    UIHelper.validationNormalTint(SignUpActivity.this, indicatorLastName);
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

    }

    public void listeners() {

        signUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (valid()) {
                    signUp();
                }

            }
        });

    }

    private boolean valid() {

        firstName = first_name.getText().toString();
        lastName = last_name.getText().toString();
        location = address.getText().toString();
        eMail = email.getText().toString();
        phoneNumber = phone.getText().toString();
        passWord = password.getText().toString();
        cPassword = cpassword.getText().toString();

        boolean fName = Validator.isValidET(firstName);
        boolean lName = Validator.isValidET(lastName);
        boolean local = Validator.isValidET(location);
        boolean mail = Validator.isValidEmail(eMail);
        boolean number = Validator.isValidPhone(phoneNumber);
        boolean pWord = Validator.isValidPassword(passWord);
        boolean CpWord = Validator.isValidPassword(cPassword);

        if (!fName) {
            UIHelper.validationFailedTint(SignUpActivity.this, indicatorFirstName);
            UIHelper.validationFailedEditText(SignUpActivity.this, first_name);

            ShowToast.showErrorMessage(getApplicationContext(), "Name(s) field invalid");

            return false;
        } else if (!lName) {
            UIHelper.validationFailedTint(SignUpActivity.this, indicatorLastName);
            UIHelper.validationFailedEditText(SignUpActivity.this, last_name);

            ShowToast.showErrorMessage(getApplicationContext(), "Name(s) field invalid");

            return false;
        } else if (!local) {
            UIHelper.validationFailedTint(SignUpActivity.this, indicatorAddress);
            UIHelper.validationFailedEditText(SignUpActivity.this, address);

            ShowToast.showErrorMessage(getApplicationContext(), "Invalid Address");

            return false;
        } else if (!mail) {
            UIHelper.validationFailedTint(SignUpActivity.this, indicatorEmail);
            UIHelper.validationFailedEditText(SignUpActivity.this, email);

            ShowToast.showErrorMessage(getApplicationContext(), "Invalid Email");

            return false;
        } else if (!number) {
            UIHelper.validationFailedTint(SignUpActivity.this, indicatorPhone);
            UIHelper.validationFailedEditText(SignUpActivity.this, phone);

            ShowToast.showErrorMessage(getApplicationContext(), "Invalid Phone Number");

            return false;
        } else if (!pWord) {
            UIHelper.validationFailedTint(SignUpActivity.this, indicatorPassword);
            UIHelper.validationFailedEditText(SignUpActivity.this, password);

            ShowToast.showErrorMessage(getApplicationContext(), "Invalid Password");

            return false;
        } else if (!CpWord) {
            UIHelper.validationFailedTint(SignUpActivity.this, indicatorCPassword);
            UIHelper.validationFailedEditText(SignUpActivity.this, cpassword);

            ShowToast.showErrorMessage(getApplicationContext(), "Invalid Confirm Password");

            return false;
        } else if (!passWord.equals(cPassword)) {
            UIHelper.validationFailedTint(SignUpActivity.this, indicatorPassword);
            UIHelper.validationFailedTint(SignUpActivity.this, indicatorCPassword);
            UIHelper.validationFailedEditText(SignUpActivity.this, password);

            ShowToast.showErrorMessage(getApplicationContext(), "Password(s) don't match");

            return false;
        }

        return true;

    }

    public void signUp() {

        Log.d(TAG, "signUp: "+firstName+lastName+location+eMail+phoneNumber+passWord);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, APICreator.REGISTER(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.d(TAG, "onResponse: " + response);

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.d(TAG, "onErrorResponse: " + error);

            }
        }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("first_name", firstName);
                params.put("last_name", lastName);
                params.put("address", location);
                params.put("email", eMail);
                params.put("phoneNumber", phoneNumber);
                params.put("password", passWord);
                params.put("f_token",fToken);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> headers = new HashMap<>();
                headers.put("Accept", "application/json");
                return headers;
            }
        };

        MySingleton.getInstance(SignUpActivity.this).addToRequestque(stringRequest);
    }
}
