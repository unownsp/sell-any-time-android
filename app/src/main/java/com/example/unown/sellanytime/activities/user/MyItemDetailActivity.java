package com.example.unown.sellanytime.activities.user;

import android.app.ProgressDialog;
import android.arch.persistence.room.Room;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.example.unown.sellanytime.R;
import com.example.unown.sellanytime.database.AppDatabase;
import com.example.unown.sellanytime.database.User;
import com.example.unown.sellanytime.network.APICreator;
import com.example.unown.sellanytime.utilities.MySingleton;
import com.example.unown.sellanytime.utilities.SharedPref;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MyItemDetailActivity extends AppCompatActivity {

    String TAG = "MyItemDetail";

    TextView nameTV, categoryTV, priceTV, descriptionTV, addressTV, infoText;
    ImageView itemImage;

    Button deleteButton;

    AppDatabase appDatabase;

    ProgressDialog progressDialog;
    String full_data, name, description, category, price, location, image, access_token;
    int item_id, reserved;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_item_detail);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        init();

        setData();

        listeners();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void init() {

        Bundle bundle = getIntent().getExtras();
        full_data = bundle.getString("detail");

        Log.d(TAG, "init: " + full_data);

        nameTV = findViewById(R.id.myItemDetail_nameTV);
        categoryTV = findViewById(R.id.myItemDetail_categoryTV);
        priceTV = findViewById(R.id.myItemDetail_priceTV);
        descriptionTV = findViewById(R.id.myItemDetail_descriptionTV);
        addressTV = findViewById(R.id.myItemDetail_addressTV);
        infoText = findViewById(R.id.myItemDetail_infoText);

        deleteButton = findViewById(R.id.myItemDetail_deleteButton);

        itemImage = findViewById(R.id.myItemDetail_image);

        progressDialog = new ProgressDialog(MyItemDetailActivity.this);

        appDatabase = Room.databaseBuilder(getApplicationContext(),
                AppDatabase.class,
                "userdb")
                .fallbackToDestructiveMigration()
                .allowMainThreadQueries()
                .build();
        List<User> user = appDatabase.myDao().getUser();

        for (User usr : user) {
            access_token = usr.getAccess_token();

            Log.d(TAG, "init: " + access_token);

        }
        appDatabase.close();
    }

    private void setData() {


        try {
            JSONObject jsonObject = new JSONObject(full_data);

            name = jsonObject.getString("name");
            description = jsonObject.getString("description");
            image = jsonObject.getString("image");
            price = jsonObject.getString("price");
            reserved = jsonObject.getInt("reserved");

            if (reserved == 1) {

                deleteButton.setVisibility(View.GONE);
                infoText.setVisibility(View.VISIBLE);

            } else {

                deleteButton.setVisibility(View.VISIBLE);
                infoText.setVisibility(View.GONE);
            }
            try {
                location = jsonObject.getString("avaliable_location");

            } catch (Exception e) {
                location = jsonObject.getString("needed_location");

            }

            try {
                category = jsonObject.getString("category");

            } catch (Exception e) {

                category = "---";

            }

            item_id = jsonObject.getInt("id");

            nameTV.setText(name);
            descriptionTV.setText(description);
            priceTV.setText(price);
            addressTV.setText(location);
            categoryTV.setText(category);

            Glide.with(getApplicationContext())
                    .load(APICreator.ITEM_IMAGE() + image)
                    .into(itemImage);

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void listeners() {

        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog alertDialog = new AlertDialog.Builder(MyItemDetailActivity.this).create();
                alertDialog.setTitle("Delete The Item?");
                alertDialog.setMessage("Are you sure you want to Delete this item?");
                alertDialog.getWindow().setBackgroundDrawableResource(R.color.white);
                alertDialog.setCanceledOnTouchOutside(false);
                alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "NO",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "YES", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        deleteMyItem();
                    }
                });
                alertDialog.show();

            }
        });
    }

    private void deleteMyItem() {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, APICreator.DELETE_MY_ITEM(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.d(TAG, "onResponse: " + response);

                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            String status = jsonObject.getString("status");

                            if (status.equals("success")) {

                                if (MyItemsActivity.myItemsActivity != null) {
                                    MyItemsActivity.myItemsActivity.taskPerforned = true;
                                }

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.d(TAG, "onErrorResponse: " + error);

            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Accept", "application/json");
                headers.put("Authorization", "Bearer " + access_token);
                return headers;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("item_id", item_id + "");
                return params;
            }
        };

        MySingleton.getInstance(getApplicationContext()).addToRequestque(stringRequest);
    }

}
