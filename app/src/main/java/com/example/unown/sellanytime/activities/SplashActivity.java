package com.example.unown.sellanytime.activities;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;

import com.example.unown.sellanytime.R;
import com.example.unown.sellanytime.activities.admin.AdminMainActivity;
import com.example.unown.sellanytime.activities.user.UserMainActivity;
import com.example.unown.sellanytime.pojos.UserDetail;
import com.example.unown.sellanytime.utilities.SharedPref;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        printKeyHash();

        SharedPref.init(this);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                boolean firstRun = SharedPref.read(SharedPref.FIRST_RUN, false);
                if (firstRun) {

                    boolean terms = SharedPref.read(SharedPref.IS_TERMS_ACCEPTED, false);

                    if (terms) {
                        if (SharedPref.read(SharedPref.IS_LOGGED_IN, false)) {

                           /* UserDetail.uid = SharedPref.read(SharedPref.UID, -1);
                            UserDetail.username = SharedPref.read(SharedPref.USERNAME, "");*/

                            boolean isUser = SharedPref.read(SharedPref.IS_USER, true);

                            if (isUser) {
                                startActivity(new Intent(SplashActivity.this, UserMainActivity.class));
                            } else {
                                startActivity(new Intent(SplashActivity.this, AdminMainActivity.class));
                            }

                        } else {
                            startActivity(new Intent(SplashActivity.this, LandingActivity.class));
                        }

                    } else {
                        startActivity(new Intent(SplashActivity.this, LoginPriorActivity.class));
                    }


                } else {
                    Intent intent = new Intent(SplashActivity.this, IntroActivity.class);
                    startActivity(intent);
                }
                finish();

            }
        }, 2000);
    }

    private void printKeyHash() {
        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    getPackageName(),
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }
    }
}
