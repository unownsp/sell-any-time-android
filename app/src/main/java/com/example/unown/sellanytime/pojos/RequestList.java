package com.example.unown.sellanytime.pojos;

public class RequestList {
    private String category;
    private int id,user_id;
    private String name;
    private String description;
    private String price;
    private String available_location;

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public int getUser_id() {return user_id;}

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getAvailable_location() {
        return available_location;
    }

    public void setAvailable_location(String available_location) {
        this.available_location = available_location;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
