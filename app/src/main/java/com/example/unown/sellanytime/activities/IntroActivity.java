package com.example.unown.sellanytime.activities;

import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.example.unown.sellanytime.R;
import com.example.unown.sellanytime.adapters.IntroAdapter;
import com.example.unown.sellanytime.utilities.SharedPref;

public class IntroActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro);

        TabLayout tabLayout = findViewById(R.id.tab_layout);
        final ViewPager viewPager = findViewById(R.id.pager);
        tabLayout.setupWithViewPager(viewPager);

        final int NO_OF_PAGES = 3;

        IntroAdapter adapter = new IntroAdapter(getSupportFragmentManager(), NO_OF_PAGES);
        viewPager.setAdapter(adapter);
        SharedPref.init(this);
        findViewById(R.id.nextTv).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //get current position of page in viewPager and check if less than count
                int pagePosition = viewPager.getCurrentItem() + 1;
                if (pagePosition < 3) viewPager.setCurrentItem(pagePosition, true);
                else {
                    startActivity(new Intent(IntroActivity.this, LoginPriorActivity.class));
                    SharedPref.write(SharedPref.FIRST_RUN, true);
                }

            }
        });

        findViewById(R.id.skipTv).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(IntroActivity.this, LoginPriorActivity.class));
                SharedPref.write(SharedPref.FIRST_RUN, true);
            }
        });
    }
}


