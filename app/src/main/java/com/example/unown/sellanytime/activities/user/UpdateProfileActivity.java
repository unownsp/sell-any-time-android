package com.example.unown.sellanytime.activities.user;

import android.arch.persistence.room.Room;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.unown.sellanytime.R;
import com.example.unown.sellanytime.customUI.ShowToast;
import com.example.unown.sellanytime.customUI.UIHelper;
import com.example.unown.sellanytime.database.AppDatabase;
import com.example.unown.sellanytime.database.User;
import com.example.unown.sellanytime.network.APICreator;
import com.example.unown.sellanytime.utilities.ImageEncoder;
import com.example.unown.sellanytime.utilities.MySingleton;
import com.example.unown.sellanytime.utilities.SharedPref;
import com.example.unown.sellanytime.utilities.Validator;
import com.myhexaville.smartimagepicker.ImagePicker;
import com.myhexaville.smartimagepicker.OnImagePickedListener;

import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UpdateProfileActivity extends AppCompatActivity {

    EditText first_name, last_name, address, password, phone, cpassword;
    TextView email;
    ImageView indicatorPhone, indicatorFirstName, indicatorLastName, indicatorAddress, indicatorPassword, indicatorCPassword;
    Button updateButton;
    String TAG = "UpdateProfileActivity";
    String firstName, lastName, location, eMail, phoneNumber, passWord, cPassword,access_token,user_id,encodedImage;
    AppDatabase appDatabase;
    ImageView profileImage,cameraImage;
    ImagePicker imagePicker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_profile);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        init();
        setData();
        listeners();
    }

    private void init() {
        first_name = findViewById(R.id.updateProfileLayout_first_nameET);
        last_name = findViewById(R.id.updateProfileLayout_last_nameET);
        address = findViewById(R.id.updateProfileLayout_addressET);
        password = findViewById(R.id.updateProfileLayout_passwordET);
        phone = findViewById(R.id.updateProfileLayout_phoneET);
        cpassword = findViewById(R.id.updateProfileLayout_CpasswordET);

        updateButton = findViewById(R.id.updateButton);

        profileImage = findViewById(R.id.updateProfileLayout_profile_picture);
        cameraImage = findViewById(R.id.updateProfileLayout_camera_icon);

        email = findViewById(R.id.updateProfileLayout_emailTV);

        appDatabase = Room.databaseBuilder(getApplicationContext(),
                AppDatabase.class,
                "userdb")
                .fallbackToDestructiveMigration()
                .allowMainThreadQueries()
                .build();
        List<User> user = appDatabase.myDao().getUser();

        for (User usr : user) {
            access_token = usr.getAccess_token();
        }

        indicatorPhone = findViewById(R.id.indicatorPhone);
        indicatorFirstName = findViewById(R.id.indicatorFirstName);
        indicatorLastName = findViewById(R.id.indicatorLastName);
        indicatorAddress = findViewById(R.id.indicatorAddress);
        indicatorPassword = findViewById(R.id.indicatorPass);
        indicatorCPassword = findViewById(R.id.indicatorCPass);


        cpassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                if (Validator.isValidPassword(charSequence)) {
                    UIHelper.validationSucessTint(UpdateProfileActivity.this, indicatorCPassword);
                } else {
                    UIHelper.validationNormalTint(UpdateProfileActivity.this, indicatorCPassword);
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        password.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (Validator.isValidPassword(charSequence)) {
                    UIHelper.validationSucessTint(UpdateProfileActivity.this, indicatorPassword);
                } else {
                    UIHelper.validationNormalTint(UpdateProfileActivity.this, indicatorPassword);
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        address.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                if (!charSequence.equals("")) {
                    UIHelper.validationSucessTint(UpdateProfileActivity.this, indicatorAddress);
                } else {
                    UIHelper.validationNormalTint(UpdateProfileActivity.this, indicatorAddress);
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        phone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                if (Validator.isValidPhone(charSequence)) {
                    UIHelper.validationSucessTint(UpdateProfileActivity.this, indicatorPhone);
                } else {
                    UIHelper.validationNormalTint(UpdateProfileActivity.this, indicatorPhone);
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        first_name.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                if (!charSequence.equals("")) {
                    UIHelper.validationSucessTint(UpdateProfileActivity.this, indicatorFirstName);
                } else {
                    UIHelper.validationNormalTint(UpdateProfileActivity.this, indicatorFirstName);
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        last_name.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                if (!charSequence.equals("")) {
                    UIHelper.validationSucessTint(UpdateProfileActivity.this, indicatorLastName);
                } else {
                    UIHelper.validationNormalTint(UpdateProfileActivity.this, indicatorLastName);
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        try {

            imagePicker = new ImagePicker(UpdateProfileActivity.this, null, new OnImagePickedListener() {
                @Override
                public void onImagePicked(Uri imageUri) {

                    System.out.println("imageUri-->" + imageUri);
                    final InputStream imageStream;
                    try {
                        imageStream = getApplicationContext().getContentResolver().openInputStream(imageUri);
                        final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
                            encodedImage = ImageEncoder.encodeImage(selectedImage);
                            profileImage.setImageURI(imageUri);
                            System.out.println("------------------------->" + encodedImage);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            }).setWithImageCrop(1, 1);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setData(){

        SharedPref.init(getApplicationContext());

        first_name.setText(SharedPref.read(SharedPref.FIRST_NAME,"---"));
        last_name.setText(SharedPref.read(SharedPref.LAST_NAME,"---"));
        address.setText(SharedPref.read(SharedPref.ADDRESS,"---"));
        phone.setText(SharedPref.read(SharedPref.PHONENUMBER,"---"));
        email.setText(SharedPref.read(SharedPref.EMAIL,"---"));
        user_id = SharedPref.read(SharedPref.UID,"-1");

    }

    private void listeners() {

        updateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (valid()){
                    update();
                }
            }
        });

        cameraImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imagePicker.choosePicture(true);

            }
        });

    }

    private boolean valid() {

        firstName = first_name.getText().toString();
        lastName = last_name.getText().toString();
        location = address.getText().toString();
        eMail = email.getText().toString();
        phoneNumber = phone.getText().toString();
        passWord = password.getText().toString();
        cPassword = cpassword.getText().toString();

        boolean fName = Validator.isValidET(firstName);
        boolean lName = Validator.isValidET(lastName);
        boolean local = Validator.isValidET(location);
        boolean number = Validator.isValidPhone(phoneNumber);
        boolean pWord = Validator.isValidPassword(passWord);
        boolean CpWord = Validator.isValidPassword(cPassword);

        if (!fName) {
            UIHelper.validationFailedTint(UpdateProfileActivity.this, indicatorFirstName);
            UIHelper.validationFailedEditText(UpdateProfileActivity.this, first_name);

            ShowToast.showErrorMessage(getApplicationContext(), "Name(s) field invalid");

            return false;
        } else if (!lName) {
            UIHelper.validationFailedTint(UpdateProfileActivity.this, indicatorLastName);
            UIHelper.validationFailedEditText(UpdateProfileActivity.this, last_name);

            ShowToast.showErrorMessage(getApplicationContext(), "Name(s) field invalid");

            return false;
        } else if (!local) {
            UIHelper.validationFailedTint(UpdateProfileActivity.this, indicatorAddress);
            UIHelper.validationFailedEditText(UpdateProfileActivity.this, address);

            ShowToast.showErrorMessage(getApplicationContext(), "Invalid Address");

            return false;
        } else if (!number) {
            UIHelper.validationFailedTint(UpdateProfileActivity.this, indicatorPhone);
            UIHelper.validationFailedEditText(UpdateProfileActivity.this, phone);

            ShowToast.showErrorMessage(getApplicationContext(), "Invalid Phone Number");

            return false;
        } else if (!pWord) {
            UIHelper.validationFailedTint(UpdateProfileActivity.this, indicatorPassword);
            UIHelper.validationFailedEditText(UpdateProfileActivity.this, password);

            ShowToast.showErrorMessage(getApplicationContext(), "Invalid Password");

            return false;
        } else if (!CpWord) {
            UIHelper.validationFailedTint(UpdateProfileActivity.this, indicatorCPassword);
            UIHelper.validationFailedEditText(UpdateProfileActivity.this, cpassword);

            ShowToast.showErrorMessage(getApplicationContext(), "Invalid Confirm Password");

            return false;
        } else if (!passWord.equals(cPassword)) {
            UIHelper.validationFailedTint(UpdateProfileActivity.this, indicatorPassword);
            UIHelper.validationFailedTint(UpdateProfileActivity.this, indicatorCPassword);
            UIHelper.validationFailedEditText(UpdateProfileActivity.this, password);

            ShowToast.showErrorMessage(getApplicationContext(), "Password(s) don't match");

            return false;
        }

        return true;

    }

    public void update() {

        Log.d(TAG, "signUp: "+firstName+lastName+location+eMail+phoneNumber+passWord);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, APICreator.UPDATE_PROFILE(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d(TAG, "onResponse: " + response);

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "onErrorResponse: " + error);

            }
        }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("first_name", firstName);
                params.put("last_name", lastName);
                params.put("address", location);
                params.put("email", eMail);
                params.put("phoneNumber", phoneNumber);
                params.put("password", passWord);
                params.put("user_id", user_id);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + access_token);
                headers.put("Accept", "application/json");
                return headers;
            }
        };
        MySingleton.getInstance(UpdateProfileActivity.this).addToRequestque(stringRequest);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
