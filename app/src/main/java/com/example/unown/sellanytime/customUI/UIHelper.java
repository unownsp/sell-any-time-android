package com.example.unown.sellanytime.customUI;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.example.unown.sellanytime.R;

/**
 * Created by unown on 8/5/2018.
 */

public class UIHelper {

    public static ProgressDialog showProgressDialog(Context context) {
        ProgressDialog pDialog = new ProgressDialog(context);
        pDialog.setMessage("Please wait...");
        pDialog.setCancelable(false);
        pDialog.show();
        return pDialog;
    }

    public static void hideProgressDialog(ProgressDialog pDialog) {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    public static void validationFailedTint(Context context, ImageView view) {
        view.setColorFilter(ContextCompat.getColor(context, R.color.colorValidationFailed), android.graphics.PorterDuff.Mode.SRC_IN);
    }

    public static void validationSucessTint(Context context, ImageView view) {
        view.setColorFilter(ContextCompat.getColor(context, R.color.colorValidationSuccess), android.graphics.PorterDuff.Mode.SRC_IN);
    }

    public static void validationNormalTint(Context context, ImageView view) {
        view.setColorFilter(ContextCompat.getColor(context, R.color.colorValidationNormal), android.graphics.PorterDuff.Mode.SRC_IN);
    }

    public static void showProgress(AppCompatActivity activity, ProgressBar progressBar) {
        progressBar.setVisibility(View.VISIBLE);
        if(activity!=null)activity.getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }
    public static void hideProgress(AppCompatActivity activity, ProgressBar progressBar) {
        progressBar.setVisibility(View.GONE);
        if(activity!=null)activity.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }
    public static void validationFailedEditText(Context context, View view) {
        YoYo.with(Techniques.Shake).playOn(view);
    }
}
