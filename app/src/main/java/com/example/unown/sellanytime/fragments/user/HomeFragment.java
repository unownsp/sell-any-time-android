package com.example.unown.sellanytime.fragments.user;


import android.app.ProgressDialog;
import android.arch.persistence.room.Room;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.unown.sellanytime.R;
import com.example.unown.sellanytime.activities.LoginActivity;
import com.example.unown.sellanytime.adapters.RecommendedAdapter;
import com.example.unown.sellanytime.database.AppDatabase;
import com.example.unown.sellanytime.database.CartItemPOJO;
import com.example.unown.sellanytime.database.User;
import com.example.unown.sellanytime.network.APICreator;
import com.example.unown.sellanytime.network.InternetConnectionCheck;
import com.example.unown.sellanytime.pojos.ItemList;
import com.example.unown.sellanytime.utilities.MySingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment {


    RecyclerView recyclerView;
    RecyclerView.LayoutManager layoutManager;
    RecyclerView.Adapter adapter;
    View view;
    ArrayList<ItemList> list = new ArrayList<>();

    String[] title,category,price;

    ProgressDialog progressDialog;

    TextView countTV;
    ImageView cardImageButton;

    MenuItem cartMenuItem;

    String TAG="HomeFragment", access_token = "";

    AppDatabase appDatabase;
    private boolean shouldCallApi = false;


    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_home, container, false);

        init();
        loadItems();
        return view;
    }

    @Override
    public void onStop() {
        super.onStop();
        shouldCallApi = true;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (shouldCallApi) {
            loadItems();
            shouldCallApi = false;
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        getActivity().getMenuInflater().inflate(R.menu.menu_default, menu);

        cartMenuItem = menu.findItem(R.id.action_cart);

        View actionVIew = cartMenuItem.getActionView();

        if (actionVIew!=null){

            countTV = actionVIew.findViewById(R.id.cart_countTV);

            cardImageButton = actionVIew.findViewById(R.id.cart_icon);

            cardImageButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Toast.makeText(getContext(),"clicked",Toast.LENGTH_SHORT).show();
                }
            });

            updateCart();


        }
    }

    private void updateCart(){
        int i=0;

        appDatabase = Room.databaseBuilder(getContext(),
                AppDatabase.class,
                "userdb")
                .fallbackToDestructiveMigration()
                .allowMainThreadQueries()
                .build();

        List<CartItemPOJO> cartItem = appDatabase.cartDao().getCartItem();

        for (CartItemPOJO cartItemPOJO :cartItem){
            i++;
        }
        if (i==0){
            countTV.setVisibility(View.INVISIBLE);
        }else{
            countTV.setVisibility(View.VISIBLE);
            countTV.setText(String.valueOf(i));
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    private void init(){

        title = getResources().getStringArray(R.array.item_title);
        category = getResources().getStringArray(R.array.item_category);
        price = getResources().getStringArray(R.array.item_price);

        appDatabase = Room.databaseBuilder(getContext(),
                AppDatabase.class,
                "userdb")
                .fallbackToDestructiveMigration()
                .allowMainThreadQueries()
                .build();
        List<User> user = appDatabase.myDao().getUser();

        for (User usr : user){
            access_token = usr.getAccess_token();

        }

        Log.d(TAG, "init: "+access_token);


    }

    private void loadItems(){

        list.clear();

        boolean internetAvailable = InternetConnectionCheck.isInternetAvailable(getContext());
        if (!internetAvailable){

            AlertDialog alertDialog = new AlertDialog.Builder(getContext()).create();
            alertDialog.getWindow().setBackgroundDrawableResource(R.color.white);
            alertDialog.setCanceledOnTouchOutside(false);
            alertDialog.setTitle(Html.fromHtml("Unable to connect"));
            alertDialog.setMessage(Html.fromHtml("Please check your internet connection."));
            alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, Html.fromHtml("ok"),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
            alertDialog.show();

            return;
        }

        progressDialog = new ProgressDialog(getContext());
        progressDialog.setTitle("Loading Items");
        progressDialog.setMessage("Please wait");
        progressDialog.show();

        StringRequest stringRequest =  new StringRequest(Request.Method.GET, APICreator.RECOMMENDATIONS(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        list.clear();
                        Log.d(TAG, "onResponse: "+response);

                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = jsonObject.getJSONArray("data");

                            for (int i = 0; i<jsonArray.length();i++){

                                JSONObject JO = jsonArray.getJSONObject(i);

                                String fullDetail = JO.toString();

                                String name = JO.getString("name");
                                String description = JO.getString("description");
                                String price = JO.getString("price");
                                String location = JO.getString("avaliable_location");
                                String image = JO.getString("image");
                                String category = JO.getString("category");

                                ItemList itemList = new ItemList(name,category,price,description,location,image,fullDetail);
                                list.add(itemList);

                            }

                            recyclerView = view.findViewById(R.id.recommendedItems);
                            recyclerView.setHasFixedSize(true);
                            layoutManager = new LinearLayoutManager(getContext());
                            recyclerView.setLayoutManager(layoutManager);
                            recyclerView.setHasFixedSize(true);

                            adapter = new RecommendedAdapter(list,getActivity());
                            recyclerView.setAdapter(adapter);

                            if (progressDialog.isShowing()){
                                progressDialog.dismiss();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();

                            if (progressDialog.isShowing()){
                                progressDialog.dismiss();
                            }
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                System.out.println("load item error "+error);

                if (progressDialog.isShowing()){
                    progressDialog.dismiss();
                }

            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Accept","application/json");
                headers.put("Authorization","Bearer "+access_token);

                return headers;
            }
        };

        MySingleton.getInstance(getContext()).addToRequestque(stringRequest);
    }

}
