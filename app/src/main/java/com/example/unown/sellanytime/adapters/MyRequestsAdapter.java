package com.example.unown.sellanytime.adapters;

import android.app.Activity;
import android.arch.persistence.room.Room;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.unown.sellanytime.R;
import com.example.unown.sellanytime.database.AppDatabase;
import com.example.unown.sellanytime.database.User;
import com.example.unown.sellanytime.network.APICreator;
import com.example.unown.sellanytime.pojos.RequestList;
import com.example.unown.sellanytime.utilities.MySingleton;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MyRequestsAdapter extends RecyclerView.Adapter<MyRequestsAdapter.ItemViewHolder>{

    private ArrayList<RequestList> itemLists = new ArrayList<>();
    private Activity mContext;
    private AppDatabase appDatabase;
    private String access_token,TAG = "MyRequestAdapter";

    public MyRequestsAdapter(ArrayList<RequestList> itemLists, Activity mContext){
        this.itemLists = itemLists;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_myrequests, parent, false);
        appDatabase = Room.databaseBuilder(mContext,
                AppDatabase.class,
                "userdb")
                .fallbackToDestructiveMigration()
                .allowMainThreadQueries()
                .build();
        List<User> user = appDatabase.myDao().getUser();

        for (User usr : user) {
            access_token = usr.getAccess_token();
            Log.d(TAG, "init: " + access_token);
        }
        appDatabase.close();
        return new ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {

        final RequestList requestList = itemLists.get(position);
        holder.category.setText(requestList.getCategory());
        holder.location.setText(requestList.getAvailable_location());
        holder.description.setText(requestList.getDescription());
        holder.price.setText(requestList.getPrice());
        holder.name.setText(requestList.getName());

        holder.deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                StringRequest stringRequest = new StringRequest(Request.Method.POST, APICreator.REQUESTS()+"/"+requestList.getId()+"",
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {

                                Log.d(TAG, "onResponse: "+response);

                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    String status =  jsonObject.getString("status");

                                    if (status.equals("success")){
                                        LayoutInflater inflater = mContext.getLayoutInflater();
                                        View layout = inflater.inflate(R.layout.custom_toast_success,(ViewGroup) mContext.findViewById(R.id.toast_root_success));

                                        TextView text = layout.findViewById(R.id.toast_success);
                                        text.setText("Request Deleted");

                                        Toast toast = new Toast(mContext);
                                        toast.setGravity(Gravity.TOP | Gravity.FILL_HORIZONTAL, 0, 0);// Set
                                        toast.setDuration(Toast.LENGTH_SHORT);// Set Duration
                                        toast.setView(layout); // Set Custom View over toast
                                        toast.show();

                                        mContext.finish();
                                    }else{
                                        LayoutInflater inflater = mContext.getLayoutInflater();
                                        View layout = inflater.inflate(R.layout.custom_toast,(ViewGroup) mContext.findViewById(R.id.toast_root));

                                        TextView text = layout.findViewById(R.id.toast_error);
                                        text.setText("Unable To Delete Request");

                                        Toast toast = new Toast(mContext);
                                        toast.setGravity(Gravity.TOP | Gravity.FILL_HORIZONTAL, 0, 0);// Set
                                        toast.setDuration(Toast.LENGTH_SHORT);// Set Duration
                                        toast.setView(layout); // Set Custom View over toast
                                        toast.show();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Log.d(TAG, "onErrorResponse: "+error);
                    }
                }){
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        Map<String, String> headers = new HashMap<>();
                        headers.put("Accept", "application/json");
                        headers.put("Authorization", "Bearer " + access_token);
                        return headers;
                    }

                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        Map<String, String> params = new HashMap<>();
                        params.put("_method", "delete");
                        return params;
                    }

                };
                MySingleton.getInstance(mContext).addToRequestque(stringRequest);

            }
        });
    }

    @Override
    public int getItemCount() {
        return itemLists.size();
    }

    public static class ItemViewHolder extends RecyclerView.ViewHolder {
        TextView name,price,description,location,category;
        Button deleteButton;
        public ItemViewHolder(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.myRequests_name);
            price = itemView.findViewById(R.id.myRequests_price);
            description = itemView.findViewById(R.id.myRequests_description);
            location = itemView.findViewById(R.id.myRequests_address);
            category = itemView.findViewById(R.id.myRequests_category);

            deleteButton = itemView.findViewById(R.id.myRequests_deleteButton);
        }
    }
}
