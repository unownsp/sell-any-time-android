package com.example.unown.sellanytime.adapters;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.unown.sellanytime.R;
import com.example.unown.sellanytime.activities.user.ItemDetailActivity;
import com.example.unown.sellanytime.network.APICreator;
import com.example.unown.sellanytime.pojos.ItemList;

import java.util.ArrayList;

public class RecommendedAdapter extends RecyclerView.Adapter<RecommendedAdapter.ItemViewHolder>{

    public ArrayList<ItemList> itemLists = new ArrayList<>();
    private Activity mContext;

    public RecommendedAdapter(ArrayList<ItemList> itemLists, Activity mContext){

        this.itemLists = itemLists;
        this.mContext = mContext;
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_recommendation,parent,false);
        ItemViewHolder itemViewHolder = new ItemViewHolder(view);
        return itemViewHolder;
    }

    @Override
    public void onBindViewHolder(ItemViewHolder holder, int position) {

        final ItemList itemList = itemLists.get(position);

        holder.title.setText(itemList.getTitle());
        holder.price.setText(itemList.getPrice());
        holder.category.setText(itemList.getCategory());
        holder.description.setText(itemList.getDescription());
        holder.location.setText(itemList.getLocation());

        Glide.with(mContext)
                .load(APICreator.ITEM_IMAGE()+itemList.getImage())
                .into(holder.item_image);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, ItemDetailActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("detail", itemList.getFull_detail());
                intent.putExtras(bundle);
                mContext.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return itemLists.size();
    }

    public static class ItemViewHolder extends RecyclerView.ViewHolder{

        TextView title,category,price,image,description,location;
        ImageView item_image;

        public ItemViewHolder(View itemView) {
            super(itemView);

            title = itemView.findViewById(R.id.recommendation_name);
            category = itemView.findViewById(R.id.recommendation_category);
            price = itemView.findViewById(R.id.recommendation_price);
            image = itemView.findViewById(R.id.recommendation_image);
            description = itemView.findViewById(R.id.recommendation_description);
            location = itemView.findViewById(R.id.recommendation_location);

            item_image = itemView.findViewById(R.id.recommendation_itemImage);
        }
    }
}
