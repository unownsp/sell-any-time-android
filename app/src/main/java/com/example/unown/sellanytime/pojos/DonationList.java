package com.example.unown.sellanytime.pojos;

/**
 * Created by unown on 10/4/2018.
 */

public class DonationList {

    private String title, price, location, image, full_detail;

    public DonationList(String title, String price, String location, String image, String full_detail) {
        this.title = title;
        this.price = price;
        this.location = location;
        this.image = image;
        this.full_detail = full_detail;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getFull_detail() {
        return full_detail;
    }

    public void setFull_detail(String full_detail) {
        this.full_detail = full_detail;
    }
}
