package com.example.unown.sellanytime.pojos;

public class RequestedItemList {

    String title,location,price,image,full_detail;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getFull_detail() {
        return full_detail;
    }

    public void setFull_detail(String full_detail) {
        this.full_detail = full_detail;
    }
}
