package com.example.unown.sellanytime.adapters;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.unown.sellanytime.R;
import com.example.unown.sellanytime.activities.user.HistoryDetailActivity;
import com.example.unown.sellanytime.network.APICreator;
import com.example.unown.sellanytime.pojos.RequestedDonationList;

import java.util.ArrayList;



public class HistoryDonationAdapter extends RecyclerView.Adapter<HistoryDonationAdapter.ItemViewHolder> {

    ArrayList<RequestedDonationList> items = new ArrayList<>();
    private Activity mContext;

    public HistoryDonationAdapter(ArrayList<RequestedDonationList> items,Activity mContext){
        this.items = items;
        this.mContext = mContext;
    }
    @NonNull
    @Override
    public HistoryDonationAdapter.ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_item_main, parent, false);
        return new ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull HistoryDonationAdapter.ItemViewHolder holder, int position) {

        final RequestedDonationList historyList =items.get(position);
        holder.location.setText(historyList.getLocation());
        holder.title.setText(historyList.getTitle());
        holder.price.setText(historyList.getPrice());

        Glide.with(mContext)
                .load(APICreator.ITEM_IMAGE() + historyList.getImage())
                .into(holder.item_image);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(mContext, HistoryDetailActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("detail", historyList.getFull_detail());
                intent.putExtras(bundle);
                mContext.startActivity(intent);

            }
        });

    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder{

        ImageView item_image;
        TextView title,price,location;

        public ItemViewHolder(View itemView) {
            super(itemView);

            title = itemView.findViewById(R.id.item_itemName);
            price = itemView.findViewById(R.id.item_itemPrice);
            location = itemView.findViewById(R.id.item_itemLocation);

            item_image = itemView.findViewById(R.id.item_itemImage);
        }
    }
}
