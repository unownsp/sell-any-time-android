package com.example.unown.sellanytime.customUI;

import android.content.Context;
import android.widget.Toast;

import com.example.unown.sellanytime.R;

public class ShowToast {

    public static void showNoInternetError(Context context) {
        try {

            Toast.makeText(context, context.getString(R.string.internet_error_message), Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void showErrorMessage(Context context, String message) {
        try {
            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void showMessage(Context context, String message) {
        try {
            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public static void showRetryingMessage(Context context) {
        try {
            Toast.makeText(context, context.getString(R.string.an_error_occurred), Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public static void showAnErrorOccurredMessage(Context context) {
        try {
            Toast.makeText(context, context.getString(R.string.an_error_occurred), Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
