package com.example.unown.sellanytime.activities.user;

import android.arch.persistence.room.Room;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.unown.sellanytime.R;
import com.example.unown.sellanytime.customUI.ShowToast;
import com.example.unown.sellanytime.database.AppDatabase;
import com.example.unown.sellanytime.database.CartItemPOJO;
import com.example.unown.sellanytime.database.User;
import com.example.unown.sellanytime.fragments.user.DonationsFragment;
import com.example.unown.sellanytime.fragments.user.HomeFragment;
import com.example.unown.sellanytime.fragments.user.ItemListFragment;
import com.example.unown.sellanytime.fragments.user.ProfileFragment;
import com.example.unown.sellanytime.fragments.user.RequestFragment;
import com.example.unown.sellanytime.network.APICreator;
import com.example.unown.sellanytime.utilities.SharedPref;

import java.util.List;

public class UserMainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    Fragment fragment;
    NavigationView navigationView;
    boolean doubleBackToExitPressedOnce;

    ImageView userImage;
    TextView welcomeText, countTV;
    ImageView cardImageButton;

    MenuItem cartMenuItem;

    AppDatabase appDatabase;
    public static UserMainActivity self;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        SharedPref.init(this);

        View header = navigationView.getHeaderView(0);
        userImage = header.findViewById(R.id.imageViewUser);
        welcomeText = header.findViewById(R.id.welcomeText);

        String welcome = "Welcome " + SharedPref.read(SharedPref.FIRST_NAME, "User");
        welcomeText.setText(welcome);

        Glide.with(this)
                .load(APICreator.PROFILE_IMAGE() + SharedPref.read(SharedPref.IMAGE, "default.jpg"))
                .into(userImage);

        fragment = new HomeFragment();
        {
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.fragments, fragment, "InitialMenuFrag")
                    .commit();
            getSupportActionBar().setTitle("Home");
            navigationView.setCheckedItem(R.id.nav_home);
        }
        getSupportFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {

            @Override
            public void onBackStackChanged() {
                Fragment f = getSupportFragmentManager().findFragmentById(R.id.fragments);
                if (f != null) {
                    if (navigationView != null) updateNavSelection(f);
                }

            }
        });


    }

    public void updateCart() {
        int i = 0;

        appDatabase = Room.databaseBuilder(getApplicationContext(),
                AppDatabase.class,
                "userdb")
                .fallbackToDestructiveMigration()
                .allowMainThreadQueries()
                .build();

        List<CartItemPOJO> cartItem = appDatabase.cartDao().getCartItem();

        for (CartItemPOJO cartItemPOJO : cartItem) {
            i++;
        }
        if (i == 0) {
            countTV.setVisibility(View.INVISIBLE);
        } else {
            countTV.setVisibility(View.VISIBLE);
            countTV.setText(String.valueOf(i));
        }
        self = this;
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            FragmentManager fragmentManager = getSupportFragmentManager();
            if (fragmentManager.findFragmentById(R.id.fragments) instanceof HomeFragment) {
                if (doubleBackToExitPressedOnce) {
                    super.onBackPressed();
                    return;
                }
                this.doubleBackToExitPressedOnce = true;
                ShowToast.showMessage(this, "Press BACK Again to exit");

                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        doubleBackToExitPressedOnce = false;
                    }
                }, 2000);
            } else {
                Fragment fragment = new HomeFragment();
                replaceFragment(fragment);
                updateNavSelection(fragment);
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_default, menu);

        cartMenuItem = menu.findItem(R.id.action_cart);

        View actionVIew = cartMenuItem.getActionView();

        if (actionVIew != null) {

            countTV = actionVIew.findViewById(R.id.cart_countTV);

            cardImageButton = actionVIew.findViewById(R.id.cart_icon);

            cardImageButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                   startActivity(new Intent(UserMainActivity.this,MyCartActivity.class));
                }
            });

            updateCart();

        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        FragmentManager fragmentManager = getSupportFragmentManager();
        Fragment currentFragment = fragmentManager.findFragmentById(R.id.fragments);

        switch (id) {
            case R.id.nav_home:
                if (currentFragment instanceof HomeFragment) fragment = null;
                else fragment = new HomeFragment();
                break;
            case R.id.nav_itemList:
                if (currentFragment instanceof ItemListFragment) fragment = null;
                else fragment = new ItemListFragment();
                break;
            case R.id.nav_donationList:
                if (currentFragment instanceof DonationsFragment) fragment = null;
                else fragment = new DonationsFragment();
                break;
            case R.id.nav_profile:
                if (currentFragment instanceof ProfileFragment) fragment = null;
                else fragment = new ProfileFragment();
                break;
            case R.id.nav_addItem:
                startActivity(new Intent(this, AddItemActivity.class));
                fragment = null;
                break;
            case R.id.nav_addDonation:
                startActivity(new Intent(this, AddDonationActivity.class));
                fragment = null;
                break;
            case R.id.nav_requests:
                if (currentFragment instanceof RequestFragment) fragment = null;
                else fragment = new RequestFragment();
                break;
            case R.id.nav_logout:
                logout();
                fragment = null;
                break;
            case R.id.nav_requestItem:
                startActivity(new Intent(getApplicationContext(), AddRequestActivity.class));
                fragment = null;
                break;
            default:
                fragment = null;
                break;
        }
        if (fragment != null) {
            replaceFragment(fragment);
            updateNavSelection(fragment);
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void logout() {
        AlertDialog alertDialog = new AlertDialog.Builder(UserMainActivity.this).create();
        alertDialog.setTitle("Order The Item?");
        alertDialog.setMessage("Are you sure you want to logout?");
        alertDialog.getWindow().setBackgroundDrawableResource(R.color.white);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "NO",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                SharedPref.init(getApplicationContext());
                SharedPref.write(SharedPref.IS_LOGGED_IN, false);
                finish();
            }
        });
        alertDialog.show();
    }

    public void replaceFragment(Fragment fragment) {

        String backStateName = fragment.getClass().getName();
        String fragmentTag = backStateName;

        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction ft = manager.beginTransaction();
        ft.replace(R.id.fragments, fragment, fragmentTag);
        ft.commit();
    }

    private void updateNavSelection(Fragment f) {

        String fragClassName = f.getClass().getName();

        if (fragClassName.equals(HomeFragment.class.getName())) {
            navigationView.setCheckedItem(R.id.nav_home);
            getSupportActionBar().setTitle("Home");
        } else if (fragClassName.equals(ItemListFragment.class.getName())) {
            navigationView.setCheckedItem(R.id.nav_itemList);
            getSupportActionBar().setTitle("Item List");
        } else if (fragClassName.equals(DonationsFragment.class.getName())) {
            navigationView.setCheckedItem(R.id.nav_itemList);
            getSupportActionBar().setTitle("Donations");
        } else if (fragClassName.equals(RequestFragment.class.getName())) {
            navigationView.setCheckedItem(R.id.nav_itemList);
            getSupportActionBar().setTitle("Requests");
        } else if (fragClassName.equals(ProfileFragment.class.getName())) {
            navigationView.setCheckedItem(R.id.nav_itemList);
            getSupportActionBar().setTitle("Profile");

        }
    }
}
