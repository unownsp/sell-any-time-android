package com.example.unown.sellanytime.adapters;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.unown.sellanytime.R;
import com.example.unown.sellanytime.activities.user.RequestedDonationDetailActivity;
import com.example.unown.sellanytime.network.APICreator;
import com.example.unown.sellanytime.pojos.RequestedDonationList;

import java.util.ArrayList;

public class RequestedDonationAdapter extends RecyclerView.Adapter<RequestedDonationAdapter.ViewHolder>{

    private ArrayList<RequestedDonationList> donationLists = new ArrayList<>();
    private Activity mContext;

    public RequestedDonationAdapter(ArrayList<RequestedDonationList> donationLists,Activity mContext){
        this.donationLists = donationLists;
        this.mContext = mContext;
    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_item_main, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        final RequestedDonationList donationList = donationLists.get(position);
        holder.title.setText(donationList.getTitle());
        holder.price.setText(donationList.getPrice());
        holder.location.setText(donationList.getLocation());

        Glide.with(mContext)
                .load(APICreator.ITEM_IMAGE() + donationList.getImage())
                .into(holder.item_image);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(mContext, RequestedDonationDetailActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("detail", donationList.getFull_detail());
                intent.putExtras(bundle);
                mContext.startActivity(intent);

            }
        });

    }

    @Override
    public int getItemCount() {
        return donationLists.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        TextView title, price, location;
        ImageView item_image;

        public ViewHolder(View itemView) {
            super(itemView);

            title = itemView.findViewById(R.id.item_itemName);
            price = itemView.findViewById(R.id.item_itemPrice);
            location = itemView.findViewById(R.id.item_itemLocation);

            item_image = itemView.findViewById(R.id.item_itemImage);
        }
    }
}
