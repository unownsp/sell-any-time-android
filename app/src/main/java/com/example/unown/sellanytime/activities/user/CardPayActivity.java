package com.example.unown.sellanytime.activities.user;

import android.arch.persistence.room.Room;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.unown.sellanytime.R;
import com.example.unown.sellanytime.database.AppDatabase;
import com.example.unown.sellanytime.network.APICreator;
import com.example.unown.sellanytime.utilities.MySingleton;
import com.example.unown.sellanytime.utilities.SharedPref;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class CardPayActivity extends AppCompatActivity {
    EditText cardNumber_ET, expireMonth_ET, expireYear_ET, cvv_ET;
    String cardNumber, email, expireMonth, expireYear, cvv, TAG = "CardPayActivity";
    Button payButton;

    TextView total_tv, subTotal_tv, shippingCost_tv;
    String total, subtotal, shippingCost, items, user_id;

    int total_money, send_money;

    AppDatabase appDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card_pay);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        init();
        setData();
        listeners();

    }

    private void init() {
        cardNumber_ET = findViewById(R.id.cardPay_cardNumberET);
        expireMonth_ET = findViewById(R.id.cardPay_expireMonthET);
        expireYear_ET = findViewById(R.id.cardPay_expireYearET);
        cvv_ET = findViewById(R.id.cardPay_cvvET);

        payButton = findViewById(R.id.payButton);

        total_tv = findViewById(R.id.cardPay_total);
        shippingCost_tv = findViewById(R.id.cardPay_shippingCost);
        subTotal_tv = findViewById(R.id.cardPay_subTotal);


    }

    private void setData() {

        Bundle bundle = getIntent().getExtras();
        total = bundle.getString("total");
        items = bundle.getString("items");
        subtotal = bundle.getString("subTotal");
        shippingCost = bundle.getString("shippingCost");
        user_id = bundle.getString("user_id");

        total_money = Integer.parseInt(total);
        send_money = total_money / 120;

        total_tv.setText(total);
        subTotal_tv.setText(subtotal);
        shippingCost_tv.setText(shippingCost);

    }

    private void listeners() {

        payButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (valid()) {
                    pay();
                }
            }
        });
    }

    private void pay() {



        StringRequest stringRequest = new StringRequest(Request.Method.POST, APICreator.CARD_PAY(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.d(TAG, "onResponse: " + response);

                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String status = jsonObject.getString("status");

                            if (status.equals("success")) {

                                AlertDialog alertDialog = new AlertDialog.Builder(CardPayActivity.this).create();
                                alertDialog.setTitle("Order Success!!");
                                alertDialog.setMessage("Clear Cart??");
                                alertDialog.getWindow().setBackgroundDrawableResource(R.color.white);
                                alertDialog.setCanceledOnTouchOutside(false);
                                alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "NO",
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {
                                                dialog.dismiss();
                                            }
                                        });
                                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "YES", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        appDatabase = Room.databaseBuilder(getApplicationContext(),
                                                AppDatabase.class,
                                                "userdb")
                                                .fallbackToDestructiveMigration()
                                                .allowMainThreadQueries()
                                                .build();

                                        appDatabase.cartDao().clearCart();

                                        if (MyCartActivity.myCartActivity != null) {
                                            MyCartActivity.myCartActivity.notifyChange();

                                            if (UserMainActivity.self != null) {
                                                UserMainActivity.self.updateCart();
                                            }
                                        }
                                    }
                                });
                                alertDialog.show();


                            } else {
                                customToastError(jsonObject.getString("message"));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.d(TAG, "onErrorResponse: " + error);

            }
        }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("items", items);
                params.put("card_no", cardNumber);
                params.put("ccExpiryYear", expireYear);
                params.put("ccExpiryMonth", expireMonth);
                params.put("cvvNumber", cvv);
                params.put("user_id", user_id);
                params.put("amount", send_money + "");
                return params;
            }

        };
        int timeout = 30000;
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(timeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MySingleton.getInstance(getApplicationContext()).addToRequestque(stringRequest);

    }

    private boolean valid() {

        cardNumber = cardNumber_ET.getText().toString();
        expireMonth = expireMonth_ET.getText().toString();
        expireYear = expireYear_ET.getText().toString();
        cvv = cvv_ET.getText().toString();

        if (cardNumber.equals("") || expireYear.equals("") || expireMonth.equals("") || cvv.equals("")) {
            customToastError("Please fill all the data!!");
            return false;
        }
        return true;
    }

    public void customToastError(String msg) {
        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.custom_toast, (ViewGroup) findViewById(R.id.toast_root));

        TextView text = layout.findViewById(R.id.toast_error);
        text.setText(msg);

        Toast toast = new Toast(getApplicationContext());
        toast.setGravity(Gravity.TOP | Gravity.FILL_HORIZONTAL, 0, 0);// Set
        toast.setDuration(Toast.LENGTH_SHORT);// Set Duration
        toast.setView(layout); // Set Custom View over toast
        toast.show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
