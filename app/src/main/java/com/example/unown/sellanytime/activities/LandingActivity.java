package com.example.unown.sellanytime.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.unown.sellanytime.R;
import com.example.unown.sellanytime.activities.user.SignUpActivity;

public class LandingActivity extends AppCompatActivity {
    Button button;
    TextView signup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_landing);

        init();
        listeners();
    }

    private void init(){

        button = findViewById(R.id.landingButton);
        //adminButton = findViewById(R.id.landingAdminButton);
        signup = findViewById(R.id.signUpTV);

    }

    private void listeners(){

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LandingActivity.this,LoginActivity.class);
                intent.putExtra("isUser",true);
                startActivity(intent);
            }
        });

        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(LandingActivity.this, SignUpActivity.class));
            }
        });

    }
}
