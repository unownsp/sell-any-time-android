package com.example.unown.sellanytime.network;

public class APICreator {

    private static String BASE_URL = "http://192.168.100.70/sellAnyTime/public";

    public static String TOKEN() {

        return BASE_URL + "/oauth/token";
    }

    public static String CURRENTUSER() {
        return BASE_URL + "/api/user";
    }

    public static String ITEMS() {
        return BASE_URL + "/api/items";
    }

    public static String RECOMMENDATIONS() {
        return BASE_URL + "/api/recommendation";
    }

    public static String REGISTER() {
        return BASE_URL + "/api/register";
    }

    public static String PROFILE_IMAGE() {
        return BASE_URL + "/uploads/profileImage/";
    }

    public static String ITEM_IMAGE() {
        return BASE_URL + "/uploads/itemImage/";
    }

    public static String DONATIONS() {
        return BASE_URL + "/api/donations";
    }

    public static String ORDER_ITEM() {
        return BASE_URL + "/api/items/";
    }

    public static String ORDER_DONATION() {
        return BASE_URL + "/api/donations/";
    }

    public static String MY_ITEMS() {
        return BASE_URL + "/api/myitems";
    }

    public static String MY_DONATIONS() {
        return BASE_URL + "/api/mydonations";
    }

    public static String ADD_ITEM() {
        return BASE_URL + "/api/items";
    }

    public static String ADD_DONATION() {
        return BASE_URL + "/api/donations";
    }

    public static String REQUESTS() {
        return BASE_URL + "/api/appeals";
    }

    public static String RESERVES() {
        return BASE_URL + "/api/reserve/";
    }

    public static String RESERVED_DONATION() {
        return BASE_URL + "/api/reservedDonation/";
    }

    public static String FORGOT_PASSWORD() {
        return BASE_URL + "/api/password/email";
    }

    public static String DELETE_MY_ITEM() {
        return BASE_URL + "/api/removeMyItem";
    }

    public static String DELETE_RESERVED_ITEM() {
        return BASE_URL + "/api/removeMyReservedItem";
    }

    public static String CHECK_OUT(){return  BASE_URL+ "/api/checkOut";}

    public static String UPDATE_PROFILE(){return BASE_URL + "/api/updateProfile";}

    public static String CARD_PAY(){return  BASE_URL+"/api/addmoney/stripe";}

    public static String DELETE_MY_DONATION(){ return BASE_URL+"/api/removeMyDonation";}

    public static String DELETE_RESERVED_DONATION() {
        return BASE_URL + "/api/removeMyReservedDonation";
    }

    public static String LOAD_REVIEW(){return BASE_URL+"/api/GetReview";}

    public static String ADD_REVIEW(){return BASE_URL+"/api/AddReview";}

}
