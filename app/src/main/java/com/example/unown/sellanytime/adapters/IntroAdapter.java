package com.example.unown.sellanytime.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.unown.sellanytime.fragments.IntroFragment;

public class IntroAdapter extends FragmentPagerAdapter {
    private int numberOfTabs;

    public IntroAdapter(FragmentManager fragmentManager, int numberOfTabs) {
        super(fragmentManager);
        this.numberOfTabs = numberOfTabs;

    }

    @Override
    public Fragment getItem(int position) {
        return IntroFragment.newInstance(position);
    }

    @Override
    public int getCount() {
        return numberOfTabs;
    }
}
