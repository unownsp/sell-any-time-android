package com.example.unown.sellanytime.fragments.user;

import android.arch.persistence.room.Room;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.unown.sellanytime.R;
import com.example.unown.sellanytime.activities.LoginActivity;
import com.example.unown.sellanytime.adapters.ItemListAdapter;
import com.example.unown.sellanytime.database.AppDatabase;
import com.example.unown.sellanytime.database.User;
import com.example.unown.sellanytime.network.APICreator;
import com.example.unown.sellanytime.network.InternetConnectionCheck;
import com.example.unown.sellanytime.pojos.ItemList;
import com.example.unown.sellanytime.pojos.ItemListMain;
import com.example.unown.sellanytime.utilities.MySingleton;
import com.example.unown.sellanytime.utilities.SharedPref;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ItemListFragment extends Fragment {

    RecyclerView recyclerView;
    RecyclerView.LayoutManager layoutManager;
    RecyclerView.Adapter adapter;
    View view;
    String TAG = "itemListFragment";
    String access_token;
    String id;
    ArrayList<ItemListMain> list = new ArrayList<>();

    String[] title, category, price;

    AppDatabase appDatabase;
    private boolean shouldCallApi = false;

    public ItemListFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_item_list, container, false);
        init();

        SharedPref.init(getContext());
        id = SharedPref.read(SharedPref.UID, "-1");

        appDatabase = Room.databaseBuilder(getContext(),
                AppDatabase.class,
                "userdb")
                .fallbackToDestructiveMigration()
                .allowMainThreadQueries()
                .build();
        List<User> user = appDatabase.myDao().getUser();

        for (User usr : user) {
            access_token = usr.getAccess_token();

        }

        getItems();

        return view;
    }

    @Override
    public void onStop() {
        super.onStop();
        shouldCallApi = true;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (shouldCallApi) {
            getItems();
            shouldCallApi = false;
        }
    }

    private void init() {
        title = getResources().getStringArray(R.array.item_title);
        category = getResources().getStringArray(R.array.item_category);
        price = getResources().getStringArray(R.array.item_price);
    }

    public void getItems() {

        boolean internetAvailable = InternetConnectionCheck.isInternetAvailable(getContext());
        if (!internetAvailable){

            AlertDialog alertDialog = new AlertDialog.Builder(getContext()).create();
            alertDialog.getWindow().setBackgroundDrawableResource(R.color.white);
            alertDialog.setCanceledOnTouchOutside(false);
            alertDialog.setTitle(Html.fromHtml("Unable to connect"));
            alertDialog.setMessage(Html.fromHtml("Please check your internet connection."));
            alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, Html.fromHtml("ok"),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
            alertDialog.show();

            return;
        }

        StringRequest stringRequest = new StringRequest(Request.Method.GET, APICreator.ITEMS(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.d(TAG, "onResponse: " + response);
                        list.clear();

                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = jsonObject.getJSONArray("data");

                            for (int i = 0; i < jsonArray.length(); i++) {

                                JSONObject JO = jsonArray.getJSONObject(i);

                                Log.d(TAG, "onResponse: " + JO.toString());

                                String full_detail = JO.toString();

                                int user_id = JO.getInt("user_id");
                                if (id.equals("" + user_id)) {
                                    continue;
                                }

                                String name = JO.getString("name");
                                int id = JO.getInt("id");
                                String description = JO.getString("description");
                                String image = JO.getString("image");
                                String avaliable_location = JO.getString("avaliable_location");
                                String category = JO.getString("category");
                                String price = JO.getString("price");
                                String user = JO.getString("user");

                                JSONObject user_detail = new JSONObject(user);
                                String user_name = user_detail.getString("first_name");
                                String user_email = user_detail.getString("email");
                                String user_image = user_detail.getString("image");


                                ItemListMain itemListMain = new ItemListMain(name, price, avaliable_location, description, image, category, user, user_name, user_email, user_image, id, full_detail);
                                list.add(itemListMain);
                            }

                            recyclerView = view.findViewById(R.id.itemListRecycler);
                            recyclerView.setHasFixedSize(true);

                            layoutManager = new LinearLayoutManager(getContext());
                            recyclerView.setLayoutManager(layoutManager);

                            adapter = new ItemListAdapter(list, getActivity());
                            recyclerView.setAdapter(adapter);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.d(TAG, "onResponse: " + error);

            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Accept", "application/json");
                headers.put("Authorization", "Bearer " + access_token);

                return headers;
            }
        };
        MySingleton.getInstance(getContext()).addToRequestque(stringRequest);
    }

}

