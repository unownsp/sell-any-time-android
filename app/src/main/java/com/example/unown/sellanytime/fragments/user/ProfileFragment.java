package com.example.unown.sellanytime.fragments.user;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.unown.sellanytime.R;
import com.example.unown.sellanytime.activities.user.HistoryActivity;
import com.example.unown.sellanytime.activities.user.HistoryDonationActivity;
import com.example.unown.sellanytime.activities.user.MyDonationsActivity;
import com.example.unown.sellanytime.activities.user.MyItemsActivity;
import com.example.unown.sellanytime.activities.user.MyRequestsActivity;
import com.example.unown.sellanytime.activities.user.RequestedDonationActivity;
import com.example.unown.sellanytime.activities.user.RequestedItemActivity;
import com.example.unown.sellanytime.activities.user.UpdateProfileActivity;
import com.example.unown.sellanytime.network.APICreator;
import com.example.unown.sellanytime.utilities.SharedPref;


public class ProfileFragment extends Fragment {

    View view;
    ImageView userImage;
    TextView userName, money;
    CardView myItemsLayout, myDonationsLayout, requestedItemLayout, historyLayout, myRequestsLayout, updateProfile;
    CardView requestedDonationLayout,historyDonationLayout;

    public ProfileFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_profile, container, false);

        init();
        listeners();

        return view;
    }

    private void init() {

        myItemsLayout = view.findViewById(R.id.myitemsLayout);
        myDonationsLayout = view.findViewById(R.id.myDonationsLayout);
        requestedItemLayout = view.findViewById(R.id.requestedItemLayout);
        historyLayout = view.findViewById(R.id.historyLayout);
        myRequestsLayout = view.findViewById(R.id.myRequestsLayout);
        updateProfile = view.findViewById(R.id.updateProfileLayout);
        requestedDonationLayout = view.findViewById(R.id.requestedDonationLayout);
        historyDonationLayout = view.findViewById(R.id.historyDonationLayout);

        userImage = view.findViewById(R.id.profile_picture);
        userName = view.findViewById(R.id.user_name);
        money = view.findViewById(R.id.user_money);

        String paisa = "Rs. " + SharedPref.read(SharedPref.WALLET, " --- ");
        money.setText(paisa);

        SharedPref.init(getContext());

        String full_name = SharedPref.read(SharedPref.FIRST_NAME, "---") + " " + SharedPref.read(SharedPref.LAST_NAME, "---");
        userName.setText(full_name);

        Glide.with(this)
                .load(APICreator.PROFILE_IMAGE() + SharedPref.read(SharedPref.IMAGE, "default.jpg"))
                .into(userImage);

    }

    private void listeners() {

        myItemsLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), MyItemsActivity.class));
            }
        });

        myDonationsLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), MyDonationsActivity.class));
            }
        });

        requestedItemLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), RequestedItemActivity.class));
            }
        });

        historyLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), HistoryActivity.class));
            }
        });

        myRequestsLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), MyRequestsActivity.class));
            }
        });


        updateProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), UpdateProfileActivity.class));
            }
        });

        requestedDonationLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(),RequestedDonationActivity.class));
            }
        });

        historyDonationLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), HistoryDonationActivity.class));

            }
        });

    }

}
