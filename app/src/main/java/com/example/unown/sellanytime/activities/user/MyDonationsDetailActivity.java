package com.example.unown.sellanytime.activities.user;

import android.app.ProgressDialog;
import android.arch.persistence.room.Room;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.example.unown.sellanytime.R;
import com.example.unown.sellanytime.database.AppDatabase;
import com.example.unown.sellanytime.database.User;
import com.example.unown.sellanytime.network.APICreator;
import com.example.unown.sellanytime.utilities.MySingleton;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MyDonationsDetailActivity extends AppCompatActivity {

    TextView nameTV, descriptionTV, addressTV,infoText;
    ImageView itemImage;
    Button deleteButton;

    AppDatabase appDatabase;

    ProgressDialog progressDialog;
    String full_data, name, description, price, location, image, access_token;
    int item_id,reserved;
    String TAG = "MyDonationDetailActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_donations_detail);

        init();

        setData();

        listeners();
    }

    private void init(){

        Bundle bundle = getIntent().getExtras();
        full_data = bundle.getString("detail");

        nameTV = findViewById(R.id.myDonationDetail_nameTV);
        descriptionTV = findViewById(R.id.myDonationDetail_descriptionTV);
        addressTV = findViewById(R.id.myDonationDetail_addressTV);

        infoText = findViewById(R.id.myDonationDetail_infoText);

        itemImage = findViewById(R.id.myDonationDetail_image);

        deleteButton = findViewById(R.id.myDonationDetail_deleteButton);

        progressDialog = new ProgressDialog(MyDonationsDetailActivity.this);

        appDatabase = Room.databaseBuilder(getApplicationContext(),
                AppDatabase.class,
                "userdb")
                .fallbackToDestructiveMigration()
                .allowMainThreadQueries()
                .build();
        List<User> user = appDatabase.myDao().getUser();

        for (User usr : user) {
            access_token = usr.getAccess_token();

            Log.d(TAG, "init: " + access_token);

        }
        appDatabase.close();
    }

    private void setData() {


        try {
            JSONObject jsonObject = new JSONObject(full_data);

            name = jsonObject.getString("name");
            description = jsonObject.getString("description");
            image = jsonObject.getString("image");
            price = jsonObject.getString("price");
            location = jsonObject.getString("needed_location");

            item_id = jsonObject.getInt("id");

            reserved = jsonObject.getInt("reserved");

            if (reserved == 1) {

                deleteButton.setVisibility(View.GONE);
                infoText.setVisibility(View.VISIBLE);

            } else {

                deleteButton.setVisibility(View.VISIBLE);
                infoText.setVisibility(View.GONE);
            }

            nameTV.setText(name);
            descriptionTV.setText(description);
            addressTV.setText(location);

            Glide.with(getApplicationContext())
                    .load(APICreator.ITEM_IMAGE() + image)
                    .into(itemImage);

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void listeners(){

        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                StringRequest stringRequest = new StringRequest(Request.Method.POST, APICreator.DELETE_MY_DONATION(),
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {

                                Log.d(TAG, "onResponse: "+response);

                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    String status = jsonObject.getString("status");
                                    if (status.equals("success")){
                                        Toast.makeText(getApplicationContext(),"Deleted",Toast.LENGTH_SHORT).show();
                                        if (MyDonationsActivity.myDonationsActivity != null){
                                            MyDonationsActivity.myDonationsActivity.getDonations();
                                        }

                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Log.d(TAG, "onErrorResponse: "+error);

                    }
                }){

                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        Map<String, String> params = new HashMap<>();
                        params.put("Accept", "application/json");
                        params.put("Authorization", "Bearer " + access_token);
                        return params;
                    }

                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        Map<String, String> params = new HashMap<>();
                        params.put("item_id", item_id + "");
                        return params;
                    }
                };

                MySingleton.getInstance(getApplicationContext()).addToRequestque(stringRequest);
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
