package com.example.unown.sellanytime.activities.user;

import android.app.ProgressDialog;
import android.arch.persistence.room.Room;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.unown.sellanytime.R;
import com.example.unown.sellanytime.adapters.CartItemsAdapter;
import com.example.unown.sellanytime.customUI.ShowToast;
import com.example.unown.sellanytime.database.AppDatabase;
import com.example.unown.sellanytime.database.CartItemPOJO;
import com.example.unown.sellanytime.database.User;
import com.example.unown.sellanytime.network.APICreator;
import com.example.unown.sellanytime.pojos.CartItemList;
import com.example.unown.sellanytime.utilities.MySingleton;
import com.example.unown.sellanytime.utilities.SharedPref;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MyCartActivity extends AppCompatActivity {

    RecyclerView cartItemsRecycler;
    TextView subTotal_tv, shippingCost_tv, tax_tv, total_tv;
    AppDatabase appDatabase;

    RecyclerView.LayoutManager layoutManager;
    RecyclerView.Adapter adapter;

    JSONArray itemJsonArray = new JSONArray();

    Button checkOutButton, walletPayButton, stripePayButton;
    String access_token, TAG = "MyCartActivity",id,wallet;
    int count = 0,userMoney=0,subTotal=0,shippingCost=0,total=0;

    ArrayList<CartItemList> list = new ArrayList<>();

    ProgressDialog progressDialog;

    public static MyCartActivity myCartActivity;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_cart);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        myCartActivity = this;
        init();
        getData();
        listeners();
    }

    private void init() {

        appDatabase = Room.databaseBuilder(getApplicationContext(),
                AppDatabase.class,
                "userdb")
                .fallbackToDestructiveMigration()
                .allowMainThreadQueries()
                .build();
        List<User> user = appDatabase.myDao().getUser();

        for (User usr : user) {
            access_token = usr.getAccess_token();

        }

        progressDialog = new ProgressDialog(MyCartActivity.this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait...");

        SharedPref.init(getApplicationContext());
        id = SharedPref.read(SharedPref.UID, "-1");

        wallet = SharedPref.read(SharedPref.WALLET,"0");
        userMoney = Integer.parseInt(wallet);

        //cardView = findViewById(R.id.cartItems_cardView);
        cartItemsRecycler = findViewById(R.id.cartItems_recycler);

        subTotal_tv = findViewById(R.id.myCart_subTotal);
        shippingCost_tv = findViewById(R.id.myCart_shippingCost);
        tax_tv = findViewById(R.id.myCart_Taxes);
        total_tv = findViewById(R.id.myCart_total);

        checkOutButton = findViewById(R.id.myCart_checkOutButton);
        walletPayButton = findViewById(R.id.myCart_payWalletButton);
        stripePayButton = findViewById(R.id.myCart_stripePayButton);

        String buttonText = "Wallet "+"("+wallet+")";
        walletPayButton.setText(buttonText);

    }

    private void listeners() {

        checkOutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkOutData();
            }
        });

        walletPayButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                walletPay();
            }
        });

        stripePayButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (count == 0){
                    customToastError("Add some item to pay!!");
                    return;
                }

                Intent intent = new Intent(MyCartActivity.this,CardPayActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("items", itemJsonArray.toString());
                bundle.putString("total",total+"");
                bundle.putString("subTotal",subTotal+"");
                bundle.putString("shippingCost",shippingCost+"");
                bundle.putString("user_id",id+"");
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });

    }

    private void getData() {

        count = 0;

        wallet = SharedPref.read(SharedPref.WALLET,"0");
        userMoney = Integer.parseInt(wallet);
        String buttonText = "Wallet "+"("+wallet+")";
        walletPayButton.setText(buttonText);

        appDatabase = Room.databaseBuilder(getApplicationContext(),
                AppDatabase.class,
                "userdb")
                .fallbackToDestructiveMigration()
                .allowMainThreadQueries()
                .build();

        List<CartItemPOJO> cartItem = appDatabase.cartDao().getCartItem();

        for (CartItemPOJO cartItemPOJO : cartItem) {

            count++;

            CartItemList cartItemList = new CartItemList();
            cartItemList.setId(cartItemPOJO.getId());
            cartItemList.setImage(cartItemPOJO.getImage());
            cartItemList.setName(cartItemPOJO.getName());
            cartItemList.setPrice("Rs " + cartItemPOJO.getPrice());

            subTotal = subTotal + Integer.parseInt(cartItemPOJO.getPrice());

            if (count % 3 == 0) {
                cartItemList.setColor("blue");
            } else if (count % 2 == 0) {
                cartItemList.setColor("red");
            } else {
                cartItemList.setColor("green");
            }
            list.add(cartItemList);

            try {

                JSONObject jsonObject = new JSONObject();
                jsonObject.put("item_id",cartItemPOJO.getId()+"");
                itemJsonArray.put(jsonObject);

            }catch (Exception e){

                System.out.print("");

            }
        }

        subTotal_tv.setText(subTotal+"");
        if (subTotal<500){
            shippingCost = 50;
            shippingCost_tv.setText(shippingCost+"");
        }else if(subTotal>500 && subTotal<1000){
            shippingCost = 100;
            shippingCost_tv.setText(shippingCost+"");
        }else if (subTotal>1000 && subTotal<1500){
            shippingCost = 120;
            shippingCost_tv.setText(shippingCost+"");
        }else{
            shippingCost = 150;
            shippingCost_tv.setText(shippingCost+"");
        }

        tax_tv.setText(" --- ");

        total = subTotal + shippingCost;
        total_tv.setText(total+"");

        if (count == 0) {
            customToastError("No Item in your cart!!");
            subTotal_tv.setText("Rs.0");
            total_tv.setText("0");
            shippingCost_tv.setText("0");
            tax_tv.setText("0");
        }

        cartItemsRecycler.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(getApplicationContext());
        cartItemsRecycler.setLayoutManager(layoutManager);

        adapter = new CartItemsAdapter(list, MyCartActivity.this);
        cartItemsRecycler.setAdapter(adapter);
    }

    public void notifyChange() {

        list.clear();
        count = 0;

        wallet = SharedPref.read(SharedPref.WALLET,"0");
        userMoney = Integer.parseInt(wallet);
        String buttonText = "Wallet "+"("+wallet+")";
        walletPayButton.setText(buttonText);

        appDatabase = Room.databaseBuilder(getApplicationContext(),
                AppDatabase.class,
                "userdb")
                .fallbackToDestructiveMigration()
                .allowMainThreadQueries()
                .build();

        List<CartItemPOJO> cartItem = appDatabase.cartDao().getCartItem();

        for (CartItemPOJO cartItemPOJO : cartItem) {

            count++;

            CartItemList cartItemList = new CartItemList();
            cartItemList.setId(cartItemPOJO.getId());
            cartItemList.setImage(cartItemPOJO.getImage());
            cartItemList.setName(cartItemPOJO.getName());
            cartItemList.setPrice("Rs " + cartItemPOJO.getPrice());

            subTotal = subTotal + Integer.parseInt(cartItemPOJO.getPrice());

            if (count % 3 == 0) {
                cartItemList.setColor("blue");
            } else if (count % 2 == 0) {
                cartItemList.setColor("red");
            } else {
                cartItemList.setColor("green");
            }
            list.add(cartItemList);

            try {

                JSONObject jsonObject = new JSONObject();
                jsonObject.put("item_id",cartItemPOJO.getId()+"");

                itemJsonArray.put(jsonObject);

            }catch (Exception e){

                System.out.print("");

            }

        }

        subTotal_tv.setText(subTotal+"");
        if (subTotal<500){
            shippingCost = 50;
            shippingCost_tv.setText(shippingCost+"");
        }else if(subTotal>500 && subTotal<1000){
            shippingCost = 100;
            shippingCost_tv.setText(shippingCost+"");
        }else if (subTotal>1000 && subTotal<1500){
            shippingCost = 120;
            shippingCost_tv.setText(shippingCost+"");
        }else{
            shippingCost = 150;
            shippingCost_tv.setText(shippingCost+"");
        }

        total = subTotal + shippingCost;
        total_tv.setText(total+"");

        if (count == 0) {
            customToastError("No Item in your cart!!");
            subTotal_tv.setText("Rs.0");
            total_tv.setText("0");
            shippingCost_tv.setText("0");
            tax_tv.setText("0");
        }

        cartItemsRecycler.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(getApplicationContext());
        cartItemsRecycler.setLayoutManager(layoutManager);

        adapter = new CartItemsAdapter(list, MyCartActivity.this);
        cartItemsRecycler.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    private void checkOutData() {

        Log.d(TAG, "checkOutData: ------------------------------------------------------------>"+itemJsonArray.toString());

        if (count==0){
            customToastError("Empty Cart");
            return;
        }

        StringRequest stringRequest = new StringRequest(Request.Method.POST, APICreator.CHECK_OUT(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.d(TAG, "onResponse: "+response);

                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String status = jsonObject.getString("status");

                            if (status.equals("Success")){
                                String msg = "success";
                                clearData(msg);
                            }
                            else{
                                String msg = "failed";
                                clearData(msg);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.d(TAG, "onErrorResponse: "+error.toString());

            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("Accept", "application/json");
                params.put("Authorization", "Bearer " + access_token);
                return params;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("user_id", id);
                params.put("items", itemJsonArray.toString());
                return params;
            }

        };
        MySingleton.getInstance(getApplicationContext()).addToRequestque(stringRequest);
    }

    private void walletPay(){

        if (count==0){
            customToastError("Empty Cart");
            return;
        }

        if (userMoney < total){
            AlertDialog alertDialog = new AlertDialog.Builder(MyCartActivity.this).create();
            alertDialog.setTitle("Not Enough Money");
            alertDialog.setMessage("Sync with server for latest data??");
            alertDialog.getWindow().setBackgroundDrawableResource(R.color.white);
            alertDialog.setCanceledOnTouchOutside(false);
            alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "NO",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
            alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "YES", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    getUserDetail();

                }
            });
            alertDialog.show();

            return;
        }

        progressDialog.setTitle("Loading your information");
        progressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, APICreator.CHECK_OUT(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.d(TAG, "onResponse: "+response);

                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String status = jsonObject.getString("status");

                            if (status.equals("Success")){
                                String msg = "success";
                                clearData(msg);
                            }
                            else{
                                String msg = "failed";
                                clearData(msg);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        if (progressDialog.isShowing()) {
                            progressDialog.dismiss();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.d(TAG, "onErrorResponse: "+error.toString());

                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("Accept", "application/json");
                params.put("Authorization", "Bearer " + access_token);
                return params;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("user_id", id);
                params.put("total",total+"");
                params.put("items", itemJsonArray.toString());
                return params;
            }

        };
        MySingleton.getInstance(getApplicationContext()).addToRequestque(stringRequest);

    }

    public void getUserDetail() {

        progressDialog.setTitle("Loading your information");
        progressDialog.show();

        StringRequest getDetail = new StringRequest(Request.Method.GET, APICreator.CURRENTUSER(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.d(TAG, "onResponse: user Detail" + response);

                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            int id = jsonObject.getInt("id");
                            int role = jsonObject.getInt("role_id");
                            String firstName = jsonObject.getString("first_name");
                            String lastName = jsonObject.getString("last_name");
                            String phoneNumber = jsonObject.getString("phoneNumber");
                            String address = jsonObject.getString("address");
                            String image = jsonObject.getString("image");
                            String email = jsonObject.getString("email");
                            String wallet = jsonObject.getString("wallet");

                            SharedPref.write(SharedPref.FIRST_NAME,firstName);
                            SharedPref.write(SharedPref.LAST_NAME,lastName);
                            SharedPref.write(SharedPref.PHONENUMBER ,phoneNumber);
                            SharedPref.write(SharedPref.ADDRESS,address);
                            SharedPref.write(SharedPref.IMAGE,image);
                            SharedPref.write(SharedPref.EMAIL,email);
                            SharedPref.write(SharedPref.ROLE_ID,role);
                            SharedPref.write(SharedPref.UID,id+"");
                            SharedPref.write(SharedPref.WALLET,wallet+"");

                            notifyChange();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        if (progressDialog.isShowing()) {
                            progressDialog.dismiss();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "onErrorResponse: user detail error " + error);

                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                if (error.toString().equals("com.android.volley.TimeoutError")) {
                    ShowToast.showErrorMessage(getApplicationContext(), "Server took too long to respond");

                    AlertDialog alertDialog = new AlertDialog.Builder(MyCartActivity.this).create();
                    alertDialog.getWindow().setBackgroundDrawableResource(R.color.white);
                    alertDialog.setCanceledOnTouchOutside(false);
                    alertDialog.setTitle(Html.fromHtml("Error"));
                    alertDialog.setMessage(Html.fromHtml("Server Did not respond"));
                    alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, Html.fromHtml("ok"),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                    alertDialog.show();
                }
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("Accept", "application/json");
                params.put("Authorization", "Bearer " + access_token);
                return params;
            }
        };

        MySingleton.getInstance(getApplicationContext()).addToRequestque(getDetail);

    }

    private void clearData(String msg){

        if (msg.equals("success")){

            AlertDialog alertDialog = new AlertDialog.Builder(MyCartActivity.this).create();
            alertDialog.setTitle("Order Success!!");
            alertDialog.setMessage("Clear Cart??");
            alertDialog.getWindow().setBackgroundDrawableResource(R.color.white);
            alertDialog.setCanceledOnTouchOutside(false);
            alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "NO",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
            alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "YES", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    appDatabase = Room.databaseBuilder(getApplicationContext(),
                            AppDatabase.class,
                            "userdb")
                            .fallbackToDestructiveMigration()
                            .allowMainThreadQueries()
                            .build();

                    appDatabase.cartDao().clearCart();
                    notifyChange();
                }
            });
            alertDialog.show();

        }else{
            AlertDialog alertDialog = new AlertDialog.Builder(MyCartActivity.this).create();
            alertDialog.setTitle("Order failed!!");
            alertDialog.setMessage("Some Items(s) in your Cart may be reserved by other user.Clear cart??");
            alertDialog.getWindow().setBackgroundDrawableResource(R.color.white);
            alertDialog.setCanceledOnTouchOutside(false);
            alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "NO",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
            alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "YES", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    appDatabase = Room.databaseBuilder(getApplicationContext(),
                            AppDatabase.class,
                            "userdb")
                            .fallbackToDestructiveMigration()
                            .allowMainThreadQueries()
                            .build();

                    appDatabase.cartDao().clearCart();
                    notifyChange();
                }
            });
            alertDialog.show();
        }

    }

    public void customToastError(String msg) {
        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.custom_toast, (ViewGroup) findViewById(R.id.toast_root));

        TextView text = layout.findViewById(R.id.toast_error);
        text.setText(msg);

        Toast toast = new Toast(getApplicationContext());
        toast.setGravity(Gravity.TOP | Gravity.FILL_HORIZONTAL, 0, 0);// Set
        toast.setDuration(Toast.LENGTH_SHORT);// Set Duration
        toast.setView(layout); // Set Custom View over toast
        toast.show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
