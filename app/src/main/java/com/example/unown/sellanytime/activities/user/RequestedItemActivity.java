package com.example.unown.sellanytime.activities.user;

import android.arch.persistence.room.Room;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.unown.sellanytime.R;
import com.example.unown.sellanytime.adapters.RequestedItemAdapter;
import com.example.unown.sellanytime.database.AppDatabase;
import com.example.unown.sellanytime.database.User;
import com.example.unown.sellanytime.network.APICreator;
import com.example.unown.sellanytime.pojos.RequestedItemList;
import com.example.unown.sellanytime.utilities.MySingleton;
import com.example.unown.sellanytime.utilities.SharedPref;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RequestedItemActivity extends AppCompatActivity {

    String TAG = "RequestedItem", access_token;
    AppDatabase appDatabase;
    String id;

    RecyclerView recyclerView;
    RecyclerView.LayoutManager layoutManager;
    RecyclerView.Adapter adapter;

    public static RequestedItemActivity requestedItemActivity;
    ArrayList<RequestedItemList> list = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_requested_item);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        init();

        getData();
        requestedItemActivity = this;

        listeners();
    }

    private void init() {
        appDatabase = Room.databaseBuilder(getApplicationContext(),
                AppDatabase.class,
                "userdb")
                .fallbackToDestructiveMigration()
                .allowMainThreadQueries()
                .build();
        List<User> user = appDatabase.myDao().getUser();

        for (User usr : user) {
            access_token = usr.getAccess_token();

            Log.d(TAG, "init: " + access_token);

        }
        appDatabase.close();

        SharedPref.init(getApplicationContext());
        id = SharedPref.read(SharedPref.UID, "-1");

    }

    public void getData() {

        list.clear();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, APICreator.RESERVES() + id,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.d(TAG, "onResponse: " + response);

                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            String status = jsonObject.getString("status");
                            if (status.equals("success")) {

                                JSONArray jsonArray = jsonObject.getJSONArray("data");

                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject JO = jsonArray.getJSONObject(i);

                                    String full_detail = JO.toString();

                                    String name = JO.getString("name");
                                    String image = JO.getString("image");
                                    String avaliable_location = JO.getString("avaliable_location");
                                    String price = JO.getString("price");
                                    int sold = JO.getInt("sold");

                                    if (sold == 1) {
                                        continue;
                                    }

                                    RequestedItemList requestedItemList = new RequestedItemList();
                                    requestedItemList.setImage(image);
                                    requestedItemList.setTitle(name);
                                    requestedItemList.setLocation(avaliable_location);
                                    requestedItemList.setPrice(price);
                                    requestedItemList.setFull_detail(full_detail);

                                    list.add(requestedItemList);
                                }

                                recyclerView = findViewById(R.id.reservedItemRecycler);
                                recyclerView.setHasFixedSize(true);

                                layoutManager = new LinearLayoutManager(getApplicationContext());
                                recyclerView.setLayoutManager(layoutManager);

                                adapter = new RequestedItemAdapter(list, RequestedItemActivity.this);
                                recyclerView.setAdapter(adapter);

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.d(TAG, "onErrorResponse: " + error.toString());

            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> headers = new HashMap<>();
                headers.put("Accept", "application/json");
                headers.put("Authorization", "Bearer " + access_token);
                return headers;
            }

        };

        MySingleton.getInstance(getApplicationContext()).addToRequestque(stringRequest);

    }

    private void listeners() {

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
