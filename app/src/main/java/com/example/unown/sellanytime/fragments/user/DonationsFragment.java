package com.example.unown.sellanytime.fragments.user;


import android.arch.persistence.room.Room;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.unown.sellanytime.R;
import com.example.unown.sellanytime.adapters.DonationsAdapter;
import com.example.unown.sellanytime.adapters.ItemListAdapter;
import com.example.unown.sellanytime.database.AppDatabase;
import com.example.unown.sellanytime.database.User;
import com.example.unown.sellanytime.network.APICreator;
import com.example.unown.sellanytime.pojos.DonationList;
import com.example.unown.sellanytime.utilities.MySingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 */
public class DonationsFragment extends Fragment {

    View view;
    AppDatabase appDatabase;
    String access_token, TAG = "Donations Fragment";

    ArrayList<DonationList> donationLists = new ArrayList<>();
    RecyclerView recyclerView;
    RecyclerView.LayoutManager layoutManager;
    RecyclerView.Adapter adapter;

    private boolean shouldCallApi = false;

    public DonationsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_donations, container, false);

        init();

        getDonations();

        return view;
    }

    @Override
    public void onStop() {
        super.onStop();
        shouldCallApi = true;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (shouldCallApi) {
            getDonations();
            shouldCallApi = false;
        }
    }

    public void init() {

        appDatabase = Room.databaseBuilder(getContext(),
                AppDatabase.class,
                "userdb")
                .fallbackToDestructiveMigration()
                .allowMainThreadQueries()
                .build();

        List<User> user = appDatabase.myDao().getUser();

        for (User usr : user) {
            access_token = usr.getAccess_token();

        }

        recyclerView = view.findViewById(R.id.DonationRecycler);
        recyclerView.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);

    }

    public void getDonations() {

        donationLists.clear();

        StringRequest stringRequest = new StringRequest(Request.Method.GET, APICreator.DONATIONS(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.d(TAG, "onResponse: " + response);

                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = jsonObject.getJSONArray("data");

                            for (int i = 0; i < jsonArray.length(); i++) {

                                JSONObject JO = jsonArray.getJSONObject(i);

                                String full_detail = JO.toString();

                                String name = JO.getString("name");
                                String image = JO.getString("image");
                                String avaliable_location = JO.getString("needed_location");
                                String price = JO.getString("price");

                                DonationList donationList = new DonationList(name, price, avaliable_location, image, full_detail);
                                donationLists.add(donationList);
                            }


                            adapter = new DonationsAdapter(donationLists,getActivity());
                            recyclerView.setAdapter(adapter);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.d(TAG, "onErrorResponse: " + error);

            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> headers = new HashMap<>();
                headers.put("Accept", "application/json");
                headers.put("Authorization", "Bearer " + access_token);
                return headers;
            }
        };

        MySingleton.getInstance(getContext()).addToRequestque(stringRequest);
    }

}
