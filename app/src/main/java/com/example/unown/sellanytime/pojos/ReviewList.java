package com.example.unown.sellanytime.pojos;

/**
 * Created by unown on 11/21/2018.
 */

public class ReviewList {
    int id;
    String review;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getReview() {
        return review;
    }

    public void setReview(String review) {
        this.review = review;
    }
}
