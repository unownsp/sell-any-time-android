package com.example.unown.sellanytime.activities.user;

import android.app.ProgressDialog;
import android.arch.persistence.room.Room;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.example.unown.sellanytime.R;
import com.example.unown.sellanytime.database.AppDatabase;
import com.example.unown.sellanytime.database.User;
import com.example.unown.sellanytime.network.APICreator;
import com.example.unown.sellanytime.utilities.MySingleton;
import com.example.unown.sellanytime.utilities.SharedPref;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RequestedDonationDetailActivity extends AppCompatActivity {

    TextView nameTV, descriptionTV, addressTV;
    ImageView itemImage;
    Button deleteButton;

    AppDatabase appDatabase;

    ProgressDialog progressDialog;
    String full_data, name, description, price, location, image, access_token,user_id;
    int item_id,reserved;
    String TAG = "MyDonationDetailActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_requested_donation_detail);

        init();

        setData();

        listeners();
    }

    private void init(){

        Bundle bundle = getIntent().getExtras();
        full_data = bundle.getString("detail");

        nameTV = findViewById(R.id.myDonationDetail_nameTV);
        descriptionTV = findViewById(R.id.myDonationDetail_descriptionTV);
        addressTV = findViewById(R.id.myDonationDetail_addressTV);


        itemImage = findViewById(R.id.myDonationDetail_image);

        deleteButton = findViewById(R.id.myDonationDetail_deleteButton);

        progressDialog = new ProgressDialog(RequestedDonationDetailActivity.this);

        appDatabase = Room.databaseBuilder(getApplicationContext(),
                AppDatabase.class,
                "userdb")
                .fallbackToDestructiveMigration()
                .allowMainThreadQueries()
                .build();
        List<User> user = appDatabase.myDao().getUser();

        for (User usr : user) {
            access_token = usr.getAccess_token();

            Log.d(TAG, "init: " + access_token);

        }
        appDatabase.close();


        SharedPref.init(getApplicationContext());
        user_id = SharedPref.read(SharedPref.UID, "-1");

    }

    private void setData() {


        try {
            JSONObject jsonObject = new JSONObject(full_data);

            name = jsonObject.getString("name");
            description = jsonObject.getString("description");
            image = jsonObject.getString("image");
            price = jsonObject.getString("price");
            location = jsonObject.getString("needed_location");

            item_id = jsonObject.getInt("id");

            reserved = jsonObject.getInt("reserved");

            nameTV.setText(name);
            descriptionTV.setText(description);
            addressTV.setText(location);

            Glide.with(getApplicationContext())
                    .load(APICreator.ITEM_IMAGE() + image)
                    .into(itemImage);

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void listeners(){

        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog alertDialog = new AlertDialog.Builder(RequestedDonationDetailActivity.this).create();
                alertDialog.setTitle("Cancel the request?");
                alertDialog.setMessage("Are you sure you want to remove this donation's request?");
                alertDialog.getWindow().setBackgroundDrawableResource(R.color.white);
                alertDialog.setCanceledOnTouchOutside(false);
                alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "NO",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "YES", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        deleteItem();
                    }
                });
                alertDialog.show();
            }
        });

    }

    private void deleteItem() {

        StringRequest stringRequest = new StringRequest(Request.Method.POST, APICreator.DELETE_RESERVED_DONATION(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.d(TAG, "onResponse: " + response);

                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String status = jsonObject.getString("status");

                            if (status.equals("success")){

                                customToastSuccess("Item Deleted from reserve");
                                if (RequestedDonationActivity.requestedDonationActivity != null){
                                    RequestedDonationActivity.requestedDonationActivity.getData();
                                }
                                finish();
                            }else{
                                customToastError("Unable to remove this item");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.d(TAG, "onErrorResponse: " + error.toString());

            }
        }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<>();
                params.put("item_id", item_id + "");
                params.put("user_id", user_id + "");
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> headers = new HashMap<>();
                headers.put("Accept", "application/json");
                headers.put("Authorization", "Bearer " + access_token);
                return headers;
            }

        };

        MySingleton.getInstance(getApplicationContext()).addToRequestque(stringRequest);
    }

    public void customToastError(String msg){
        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.custom_toast,(ViewGroup) findViewById(R.id.toast_root));

        TextView text = layout.findViewById(R.id.toast_error);
        text.setText(msg);

        Toast toast = new Toast(getApplicationContext());
        toast.setGravity(Gravity.TOP | Gravity.FILL_HORIZONTAL, 0, 0);// Set
        toast.setDuration(Toast.LENGTH_SHORT);// Set Duration
        toast.setView(layout); // Set Custom View over toast
        toast.show();
    }

    public void customToastSuccess(String msg){
        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.custom_toast_success,(ViewGroup) findViewById(R.id.toast_root_success));

        TextView text = layout.findViewById(R.id.toast_success);
        text.setText(msg);

        Toast toast = new Toast(getApplicationContext());
        toast.setGravity(Gravity.TOP | Gravity.FILL_HORIZONTAL, 0, 0);// Set
        toast.setDuration(Toast.LENGTH_SHORT);// Set Duration
        toast.setView(layout); // Set Custom View over toast
        toast.show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
