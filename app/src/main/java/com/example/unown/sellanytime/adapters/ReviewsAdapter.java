package com.example.unown.sellanytime.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.unown.sellanytime.R;
import com.example.unown.sellanytime.pojos.ReviewList;

import java.util.ArrayList;

public class ReviewsAdapter extends RecyclerView.Adapter<ReviewsAdapter.ItemViewHolder> {

    ArrayList<ReviewList> items = new ArrayList<>();

    public ReviewsAdapter(ArrayList<ReviewList> items){
        this.items = items;
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_review, parent, false);
        ItemViewHolder itemViewHolder = new ItemViewHolder(view);
        return itemViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {

        ReviewList reviewList = items.get(position);
        holder.reviewTV.setText(reviewList.getReview());

    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder{

        TextView reviewTV;

        public ItemViewHolder(View itemView) {
            super(itemView);

            reviewTV = itemView.findViewById(R.id.addedBy_row_reviewTv);
        }
    }
}
