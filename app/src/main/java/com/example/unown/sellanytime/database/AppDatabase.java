package com.example.unown.sellanytime.database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;


@Database(entities = {User.class,CartItemPOJO.class},version=1)
public abstract class AppDatabase extends RoomDatabase{

    public abstract MyDao myDao();

    public abstract CartItemDao cartDao();
}
