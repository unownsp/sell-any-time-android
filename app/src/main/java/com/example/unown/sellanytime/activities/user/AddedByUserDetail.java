package com.example.unown.sellanytime.activities.user;

import android.app.ProgressDialog;
import android.arch.persistence.room.Room;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.example.unown.sellanytime.R;
import com.example.unown.sellanytime.adapters.ReviewsAdapter;
import com.example.unown.sellanytime.database.AppDatabase;
import com.example.unown.sellanytime.database.User;
import com.example.unown.sellanytime.network.APICreator;
import com.example.unown.sellanytime.pojos.ReviewList;
import com.example.unown.sellanytime.utilities.MySingleton;
import com.example.unown.sellanytime.utilities.SharedPref;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AddedByUserDetail extends AppCompatActivity {

    ImageView userImage;
    TextView name, address, email;
    EditText reviewET;
    String user, access_token, TAG = "AddedByUserDetail",reviewText,by_id;
    AppDatabase appDatabase;
    Button buttonLoadReview, addReviewButton;
    LinearLayout reviewsLayout;
    int user_id;

    ProgressDialog progressDialog;

    RecyclerView recyclerView;
    RecyclerView.LayoutManager layoutManager;
    RecyclerView.Adapter adapter;
    ArrayList<ReviewList> list = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_added_by_user_detail);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        init();
        setData();
        listeners();

    }

    private void init() {

        userImage = findViewById(R.id.addedBy_userImage);
        name = findViewById(R.id.addedBy_userName);
        address = findViewById(R.id.addedBy_userAddress);
        email = findViewById(R.id.addedBy_userEmail);

        buttonLoadReview = findViewById(R.id.addedBy_loadReviewsButton);
        addReviewButton = findViewById(R.id.addedBy_addReviewButton);

        reviewsLayout = findViewById(R.id.addedBy_reviewsLayout);

        reviewET = findViewById(R.id.addedBy_reviewET);

        SharedPref.init(getApplicationContext());
        by_id = SharedPref.read(SharedPref.UID, "-1");

        Bundle bundle = getIntent().getExtras();
        user = bundle.getString("user");

        progressDialog = new ProgressDialog(AddedByUserDetail.this);
        progressDialog.setTitle("Loading Items");
        progressDialog.setMessage("Please wait");

        appDatabase = Room.databaseBuilder(getApplicationContext(),
                AppDatabase.class,
                "userdb")
                .fallbackToDestructiveMigration()
                .allowMainThreadQueries()
                .build();
        List<User> user = appDatabase.myDao().getUser();

        for (User usr : user) {
            access_token = usr.getAccess_token();

        }

    }

    private void setData() {

        try {
            JSONObject jsonObject = new JSONObject(user);
            String fN = jsonObject.getString("first_name");
            String lN = jsonObject.getString("last_name");
            String eMail = jsonObject.getString("email");
            String location = jsonObject.getString("address");
            String image = jsonObject.getString("image");
            user_id = jsonObject.getInt("id");

            String full_name = fN + " " + lN;

            name.setText(full_name);
            email.setText(eMail);
            address.setText(location);

            Glide.with(getApplicationContext()).load(APICreator.PROFILE_IMAGE() + image).into(userImage);

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void listeners() {
        buttonLoadReview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                loadReviews();
                reviewsLayout.setVisibility(View.VISIBLE);
                buttonLoadReview.setVisibility(View.INVISIBLE);

            }
        });

        addReviewButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                 if (valid()){
                     addReview();
                 }
            }
        });

    }

    private void loadReviews() {

        list.clear();

        if (!progressDialog.isShowing()){
            progressDialog.show();
        }


        StringRequest stringRequest = new StringRequest(Request.Method.POST, APICreator.LOAD_REVIEW(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.d(TAG, "onResponse: " + response);

                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            String status = jsonObject.getString("status");
                            if (status.equals("success")) {

                                JSONArray jsonArray = jsonObject.getJSONArray("data");

                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject JO = jsonArray.getJSONObject(i);
                                    String review = JO.getString("review");
                                    int id = JO.getInt("id");

                                    ReviewList reviewList = new ReviewList();
                                    reviewList.setId(id);
                                    reviewList.setReview(review);

                                    list.add(reviewList);
                                }

                                recyclerView = findViewById(R.id.addedBy_reviewRecycler);
                                recyclerView.setHasFixedSize(true);

                                layoutManager = new LinearLayoutManager(getApplicationContext());
                                recyclerView.setLayoutManager(layoutManager);

                                adapter = new ReviewsAdapter(list);
                                recyclerView.setAdapter(adapter);
                            }

                            if (progressDialog.isShowing()){
                                progressDialog.dismiss();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                if (progressDialog.isShowing()){
                    progressDialog.dismiss();
                }

                Log.d(TAG, "onErrorResponse: " + error);

            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("Accept", "application/json");
                params.put("Authorization", "Bearer " + access_token);
                return params;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("to_id", user_id + "");
                return params;
            }

        };
        MySingleton.getInstance(getApplicationContext()).addToRequestque(stringRequest);
    }

    private void addReview(){

        StringRequest stringRequest = new StringRequest(Request.Method.POST, APICreator.ADD_REVIEW(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d(TAG, "onResponse: "+response);

                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String status = jsonObject.getString("status");

                            if (status.equals("success")){
                                loadReviews();
                                reviewET.setText("");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {


                Log.d(TAG, "onErrorResponse: "+error.toString());
            }
        }){

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("Accept", "application/json");
                params.put("Authorization", "Bearer " + access_token);
                return params;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("to_id", user_id + "");
                params.put("by_id",by_id);
                params.put("review",reviewText);
                return params;
            }


        };

        MySingleton.getInstance(getApplicationContext()).addToRequestque(stringRequest);
    }

    private boolean valid(){

        reviewText = reviewET.getText().toString();

        if (reviewText.equals("")){
            Toast.makeText(getApplicationContext(),"Add some review",Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
