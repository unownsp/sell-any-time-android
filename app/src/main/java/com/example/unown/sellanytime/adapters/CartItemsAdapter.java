package com.example.unown.sellanytime.adapters;

import android.app.Activity;
import android.arch.persistence.room.Room;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.unown.sellanytime.R;
import com.example.unown.sellanytime.activities.user.MyCartActivity;
import com.example.unown.sellanytime.activities.user.UserMainActivity;
import com.example.unown.sellanytime.database.AppDatabase;
import com.example.unown.sellanytime.database.CartItemPOJO;
import com.example.unown.sellanytime.network.APICreator;
import com.example.unown.sellanytime.pojos.CartItemList;

import java.util.ArrayList;

public class CartItemsAdapter extends RecyclerView.Adapter<CartItemsAdapter.ItemViewHolder> {

    private ArrayList<CartItemList> itemLists = new ArrayList<>();
    private Activity mContext;
    private AppDatabase appDatabase;

    public CartItemsAdapter(ArrayList<CartItemList> itemLists, Activity mContext) {

        this.itemLists = itemLists;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_mycart_item, parent, false);
        appDatabase = Room.databaseBuilder(mContext,
                AppDatabase.class,
                "userdb")
                .fallbackToDestructiveMigration()
                .allowMainThreadQueries()
                .build();
        return new ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, final int position) {

        final CartItemList cartItemList = itemLists.get(position);

        holder.name.setText(cartItemList.getName());
        holder.price.setText(cartItemList.getPrice());

        Glide.with(mContext)
                .load(APICreator.ITEM_IMAGE() + cartItemList.getImage())
                .into(holder.itemImage);

        if (cartItemList.getColor().equals("red")){

            holder.colorIndicator.setImageResource(R.drawable.rounded_red);

        }else if (cartItemList.getColor().equals("blue")){

            holder.colorIndicator.setImageResource(R.drawable.rounded_blue);

        }else if (cartItemList.getColor().equals("green")){

            holder.colorIndicator.setImageResource(R.drawable.rounded_green);

        }

        holder.removeItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog alertDialog = new AlertDialog.Builder(mContext).create();
                alertDialog.setTitle("Remove Form Cart??");
                alertDialog.setMessage("Are you sure you want to remove this item from cart?");
                alertDialog.getWindow().setBackgroundDrawableResource(R.color.white);
                alertDialog.setCanceledOnTouchOutside(false);
                alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "NO",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "YES", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        try {
                            CartItemPOJO toDeleteItem = new CartItemPOJO();
                            toDeleteItem.setId(cartItemList.getId());

                            appDatabase.cartDao().deleteItem(toDeleteItem);
                            Toast.makeText(mContext, "Item Removed From Cart", Toast.LENGTH_SHORT).show();

                            itemLists.remove(position);

                            if (MyCartActivity.myCartActivity != null){
                                MyCartActivity.myCartActivity.notifyChange();
                            }


                        } catch (Exception e) {

                            Log.d("cartitemdelete", "onClick: "+e.toString());
                            Toast.makeText(mContext, "Unable to Remove Item", Toast.LENGTH_SHORT).show();
                        }


                        if (UserMainActivity.self != null) {
                            UserMainActivity.self.updateCart();
                        }
                    }
                });
                alertDialog.show();
            }
        });

    }

    @Override
    public int getItemCount() {
        return itemLists.size();
    }

    class ItemViewHolder extends RecyclerView.ViewHolder{

        TextView name,price;
        ImageView itemImage,colorIndicator,removeItem;

        public ItemViewHolder(View itemView) {
            super(itemView);

            name = itemView.findViewById(R.id.cartItem_itemName);
            price = itemView.findViewById(R.id.cartItem_itemCost);

            itemImage = itemView.findViewById(R.id.cartItem_itemImage);
            colorIndicator = itemView.findViewById(R.id.cartItem_colorIndicator);
            removeItem = itemView.findViewById(R.id.cartItem_removeItem);
        }
    }
}
