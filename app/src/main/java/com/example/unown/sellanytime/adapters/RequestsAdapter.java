package com.example.unown.sellanytime.adapters;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.unown.sellanytime.R;
import com.example.unown.sellanytime.pojos.RequestList;

import java.util.ArrayList;

public class RequestsAdapter extends RecyclerView.Adapter<RequestsAdapter.ItemViewHolder>{

    private ArrayList<RequestList> itemLists = new ArrayList<>();
    private Activity mContext;

    public RequestsAdapter(ArrayList<RequestList> itemLists, Activity mContext){
        this.itemLists = itemLists;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_requests, parent, false);
        ItemViewHolder itemViewHolder = new ItemViewHolder(view);
        return itemViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {

        RequestList requestList = itemLists.get(position);
        holder.category.setText(requestList.getCategory());
        holder.location.setText(requestList.getAvailable_location());
        holder.description.setText(requestList.getDescription());
        holder.price.setText(requestList.getPrice());
        holder.name.setText(requestList.getName());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                    LayoutInflater inflater = mContext.getLayoutInflater();
                    View layout = inflater.inflate(R.layout.custom_toast_success,(ViewGroup) mContext.findViewById(R.id.toast_root_success));

                    TextView text = layout.findViewById(R.id.toast_success);
                    text.setText("Add item with similar name and category to respond");

                    Toast toast = new Toast(mContext);
                    toast.setGravity(Gravity.TOP | Gravity.FILL_HORIZONTAL, 0, 0);// Set
                    toast.setDuration(Toast.LENGTH_SHORT);// Set Duration
                    toast.setView(layout); // Set Custom View over toast
                    toast.show();

            }
        });
    }

    @Override
    public int getItemCount() {
        return itemLists.size();
    }

    public static class ItemViewHolder extends RecyclerView.ViewHolder {

        TextView name,price,description,location,category;

        public ItemViewHolder(View itemView) {
            super(itemView);

            name = itemView.findViewById(R.id.requests_name);
            price = itemView.findViewById(R.id.requests_price);
            description = itemView.findViewById(R.id.requests_description);
            location = itemView.findViewById(R.id.requests_address);
            category = itemView.findViewById(R.id.requests_category);
        }
    }
}
