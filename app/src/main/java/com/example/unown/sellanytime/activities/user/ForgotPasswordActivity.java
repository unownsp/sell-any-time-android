package com.example.unown.sellanytime.activities.user;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.example.unown.sellanytime.R;
import com.example.unown.sellanytime.network.APICreator;
import com.example.unown.sellanytime.utilities.MySingleton;
import com.example.unown.sellanytime.utilities.Validator;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ForgotPasswordActivity extends AppCompatActivity {

    ImageView backImage;
    Button resetButton;
    EditText emailET;
    String email;

    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        backImage = findViewById(R.id.close_forgot);
        backImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        progressDialog = new ProgressDialog(ForgotPasswordActivity.this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait...");

        emailET = findViewById(R.id.registered_emailid);

        emailET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }
//--------------------------------------------------------------------------------------------------Metal
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (Validator.isValidEmail(charSequence)) {
                    /*UIHelper.validationSucessTint(LoginActivity.this, indicatorEmail);*/

                    emailET.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.email,0, R.drawable.indicator_icon_valid,0);

                } else {
                    /*UIHelper.validationNormalTint(LoginActivity.this, indicatorEmail);*/

                    emailET.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.email,0, R.drawable.indicator_icon,0);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
//--------------------------------------------------------------------------------------------------Metal
        resetButton = findViewById(R.id.resetButton);

        resetButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (valid()){

                    resetPassword();
                }
            }
        });

    }

    private boolean valid(){

        email = emailET.getText().toString();
//--------------------------------------------------------------------------------------------------metal
        boolean valid_email = Validator.isValidEmail(email);

        if (!valid_email){
            emailET.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.email,0, R.drawable.indicator_icon_failed,0);
            YoYo.with(Techniques.Shake).playOn(emailET);
            Toast.makeText(getApplicationContext(),"Invalid Email",Toast.LENGTH_SHORT).show();

            return false;
        }
//--------------------------------------------------------------------------------------------------metal
        return true;
    }

    private void resetPassword(){

        progressDialog.setTitle("Sending reset link");
        progressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, APICreator.FORGOT_PASSWORD(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        if (progressDialog.isShowing()) {
                            progressDialog.dismiss();
                        }

                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String status = jsonObject.getString("status");

                            if (status.equals("success")){

                                Toast.makeText(getApplicationContext(),"Reset link sent",Toast.LENGTH_SHORT).show();
                                finish();
                            }else {

                                Toast.makeText(getApplicationContext(),"Unable to send reset link",Toast.LENGTH_SHORT).show();

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Log.d("forgot", "onResponse: "+response);

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                Log.d("forgot", "onErrorResponse: "+error);
                Toast.makeText(getApplicationContext(),"Unable to send reset link",Toast.LENGTH_SHORT).show();

            }
        }){

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> headers = new HashMap<>();
                headers.put("Accept", "application/json");
                return headers;
            }

            @Override
            public Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<>();
                params.put("email", email);
                return params;
            }

        };
        int timeout = 30000;
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(timeout,DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MySingleton.getInstance(getApplicationContext()).addToRequestque(stringRequest);
    }
}
