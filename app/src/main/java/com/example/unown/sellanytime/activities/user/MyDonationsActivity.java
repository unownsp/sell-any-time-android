package com.example.unown.sellanytime.activities.user;

import android.arch.persistence.room.Room;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.unown.sellanytime.R;
import com.example.unown.sellanytime.adapters.MyDonationsAdapter;
import com.example.unown.sellanytime.database.AppDatabase;
import com.example.unown.sellanytime.database.User;
import com.example.unown.sellanytime.network.APICreator;
import com.example.unown.sellanytime.pojos.DonationList;
import com.example.unown.sellanytime.utilities.MySingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MyDonationsActivity extends AppCompatActivity {

    AppDatabase appDatabase;
    String access_token, TAG = "MyDonationsActivity";
    ArrayList<DonationList> list = new ArrayList<>();

    RecyclerView recyclerView;
    RecyclerView.LayoutManager layoutManager;
    RecyclerView.Adapter adapter;

    public static MyDonationsActivity myDonationsActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_donations);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        init();

        myDonationsActivity = this;

       getDonations();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void init(){
        appDatabase = Room.databaseBuilder(getApplicationContext(),
                AppDatabase.class,
                "userdb")
                .fallbackToDestructiveMigration()
                .allowMainThreadQueries()
                .build();
        List<User> user = appDatabase.myDao().getUser();

        for (User usr : user) {
            access_token = usr.getAccess_token();

        }

        recyclerView = findViewById(R.id.MyDonationsRecycler);
        recyclerView.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
    }

    public void getDonations() {

        StringRequest stringRequest = new StringRequest(Request.Method.GET, APICreator.MY_DONATIONS(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.d(TAG, "onResponse: " + response);

                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = jsonObject.getJSONArray("data");

                            for (int i = 0; i < jsonArray.length(); i++) {

                                JSONObject JO = jsonArray.getJSONObject(i);

                                String full_detail = JO.toString();

                                String name = JO.getString("name");
                                String image = JO.getString("image");
                                String avaliable_location = JO.getString("needed_location");
                                String price = JO.getString("price");

                                DonationList donationList = new DonationList(name, price, avaliable_location, image, full_detail);
                                list.add(donationList);
                            }


                            adapter = new MyDonationsAdapter(list,MyDonationsActivity.this);
                            recyclerView.setAdapter(adapter);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.d(TAG, "onErrorResponse: " + error);

            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> headers = new HashMap<>();
                headers.put("Accept", "application/json");
                headers.put("Authorization", "Bearer " + access_token);
                return headers;
            }
        };

        MySingleton.getInstance(getApplicationContext()).addToRequestque(stringRequest);
    }
}
