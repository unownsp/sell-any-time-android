package com.example.unown.sellanytime.pojos;

public class ItemImagesList {

    String image;

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

}
