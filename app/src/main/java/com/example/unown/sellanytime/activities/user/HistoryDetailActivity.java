package com.example.unown.sellanytime.activities.user;

import android.arch.persistence.room.Room;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.example.unown.sellanytime.R;
import com.example.unown.sellanytime.database.AppDatabase;
import com.example.unown.sellanytime.database.User;
import com.example.unown.sellanytime.network.APICreator;
import com.example.unown.sellanytime.utilities.MySingleton;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HistoryDetailActivity extends AppCompatActivity {

    ImageView itemImage;
    TextView nameTV, categoryTV, priceTV, descriptionTV, addressTV;

    String full_data, name, description, category, price, location, image, access_token, user,TAG = "HistoryDetail";
    int item_id;
    AppDatabase appDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history_detail);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        init();

        setData();

        listeners();
    }

    private void init(){

        Bundle bundle = getIntent().getExtras();
        full_data = bundle.getString("detail");

        appDatabase = Room.databaseBuilder(getApplicationContext(),
                AppDatabase.class,
                "userdb")
                .fallbackToDestructiveMigration()
                .allowMainThreadQueries()
                .build();
        List<User> user = appDatabase.myDao().getUser();

        for (User usr : user) {
            access_token = usr.getAccess_token();

            Log.d(TAG, "init: " + access_token);

        }
        appDatabase.close();

        itemImage = findViewById(R.id.RequestedItemDetail_image);
        nameTV = findViewById(R.id.requestedItemDetail_nameTV);
        categoryTV = findViewById(R.id.requestedItemDetail_categoryTV);
        priceTV = findViewById(R.id.requestedItemDetail_priceTV);
        descriptionTV = findViewById(R.id.requestedItemDetail_descriptionTV);
        addressTV = findViewById(R.id.requestedItemDetail_addressTV);

    }

    private void setData(){

        try {
            JSONObject jsonObject = new JSONObject(full_data);

            name = jsonObject.getString("name");
            description = jsonObject.getString("description");
            image = jsonObject.getString("image");
            price = jsonObject.getString("price");

            try {
                location = jsonObject.getString("avaliable_location");

            } catch (Exception e) {
                location = jsonObject.getString("needed_location");

            }

            try {
                category = jsonObject.getString("category");

            } catch (Exception e) {

                category = "---";

            }

            item_id = jsonObject.getInt("id");

            nameTV.setText(name);
            descriptionTV.setText(description);
            priceTV.setText(price);
            addressTV.setText(location);
            categoryTV.setText(category);

            Glide.with(getApplicationContext())
                    .load(APICreator.ITEM_IMAGE() + image)
                    .into(itemImage);

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void listeners(){
    }
}
