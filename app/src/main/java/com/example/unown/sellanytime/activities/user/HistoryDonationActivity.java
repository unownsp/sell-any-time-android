package com.example.unown.sellanytime.activities.user;

import android.arch.persistence.room.Room;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.unown.sellanytime.R;
import com.example.unown.sellanytime.adapters.HistoryDonationAdapter;
import com.example.unown.sellanytime.adapters.RequestedDonationAdapter;
import com.example.unown.sellanytime.database.AppDatabase;
import com.example.unown.sellanytime.database.User;
import com.example.unown.sellanytime.network.APICreator;
import com.example.unown.sellanytime.pojos.RequestedDonationList;
import com.example.unown.sellanytime.utilities.MySingleton;
import com.example.unown.sellanytime.utilities.SharedPref;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HistoryDonationActivity extends AppCompatActivity {

    String TAG = "HistoryDonationActivity", access_token;
    AppDatabase appDatabase;
    String id;

    RecyclerView recyclerView;
    RecyclerView.LayoutManager layoutManager;
    RecyclerView.Adapter adapter;


    public static HistoryDetailActivity historyDetailActivity;
    ArrayList<RequestedDonationList> list = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history_donation);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        init();

        getData();
    }

    private void init() {

        appDatabase = Room.databaseBuilder(getApplicationContext(),
                AppDatabase.class,
                "userdb")
                .fallbackToDestructiveMigration()
                .allowMainThreadQueries()
                .build();
        List<User> user = appDatabase.myDao().getUser();

        for (User usr : user) {
            access_token = usr.getAccess_token();

            Log.d(TAG, "init: " + access_token);

        }
        appDatabase.close();

        SharedPref.init(getApplicationContext());
        id = SharedPref.read(SharedPref.UID, "-1");

    }

    public void getData() {

        list.clear();

        Log.d(TAG, "getData: -------------------------------------------------------------"+ APICreator.RESERVED_DONATION() + id);

        StringRequest stringRequest = new StringRequest(Request.Method.GET, APICreator.RESERVED_DONATION() + id,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.d(TAG, "onResponse: " + response);

                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            String status = jsonObject.getString("status");
                            if (status.equals("success")) {
                                JSONArray jsonArray = jsonObject.getJSONArray("data");
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject JO = jsonArray.getJSONObject(i);

                                    String full_detail = JO.toString();

                                    String name = JO.getString("name");
                                    String image = JO.getString("image");
                                    String avaliable_location = JO.getString("needed_location");
                                    String price = JO.getString("price");
                                    int sold = JO.getInt("sold");

                                    if (sold == 0) {
                                        continue;
                                    }

                                    RequestedDonationList requestedDonationList = new RequestedDonationList();
                                    requestedDonationList.setImage(image);
                                    requestedDonationList.setTitle(name);
                                    requestedDonationList.setLocation(avaliable_location);
                                    requestedDonationList.setPrice(price);
                                    requestedDonationList.setFull_detail(full_detail);

                                    list.add(requestedDonationList);
                                }

                                recyclerView = findViewById(R.id.historyDonationRecycler);
                                recyclerView.setHasFixedSize(true);

                                layoutManager = new LinearLayoutManager(getApplicationContext());
                                recyclerView.setLayoutManager(layoutManager);

                                adapter = new HistoryDonationAdapter(list, HistoryDonationActivity.this);
                                recyclerView.setAdapter(adapter);

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.d(TAG, "onErrorResponse: " + error.toString());

            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> headers = new HashMap<>();
                headers.put("Accept", "application/json");
                headers.put("Authorization", "Bearer " + access_token);
                return headers;
            }

        };

        MySingleton.getInstance(getApplicationContext()).addToRequestque(stringRequest);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
