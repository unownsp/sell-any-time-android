package com.example.unown.sellanytime.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import com.example.unown.sellanytime.R;
import com.example.unown.sellanytime.activities.user.SignUpActivity;
import com.example.unown.sellanytime.customUI.ShowToast;
import com.example.unown.sellanytime.utilities.SharedPref;

public class LoginPriorActivity extends AppCompatActivity {

    Button accept;
    CheckBox checkBox;
    TextView termsAndCondition, signUp;
    CardView cardView;
    Animation animation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_prior);

        init();
        listeners();
    }

    private void init() {

        accept = findViewById(R.id.acceptButton);

        cardView = findViewById(R.id.titleCard);

        animation = AnimationUtils.loadAnimation(LoginPriorActivity.this,R.anim.card_anim);
        cardView.setAnimation(animation);

        checkBox = findViewById(R.id.loginPriorCheckBox);

        termsAndCondition = findViewById(R.id.termsAndConditionsTV);
        signUp = findViewById(R.id.signUpTV);

        SharedPref.init(LoginPriorActivity.this);

    }

    private void listeners() {

        accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(LoginPriorActivity.this,SignUpActivity.class));

            }
        });

        signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                SharedPref.write(SharedPref.IS_TERMS_ACCEPTED,true);
                startActivity(new Intent(LoginPriorActivity.this, LoginActivity.class));

            }
        });

        termsAndCondition.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ShowToast.showErrorMessage(LoginPriorActivity.this,"terms and condition will be shown soon :D");
            }
        });

    }
}
