package com.example.unown.sellanytime.utilities;

import android.text.TextUtils;
import android.util.Patterns;


public class Validator {

    public static int PASSWORD_LENGTH = 6;
    public static int PHONE_LENGTH_MIN = 10;
    public static int PHONE_LENGTH_MAX = 13;

    public final static boolean isValidEmail(CharSequence target) {
        if (target != null)
            return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
        else return false;
    }

    public final static boolean isValidPassword(CharSequence s) {
        return (!TextUtils.isEmpty(s) && s.length() >= PASSWORD_LENGTH);
    }

    public final static boolean isValidPhone(CharSequence s) {
        return (!TextUtils.isEmpty(s) && s.length() > PHONE_LENGTH_MIN && s.length() < PHONE_LENGTH_MAX);
    }

    public final static boolean isValidET(CharSequence s) {
        return (!TextUtils.isEmpty(s) && TextUtils.getTrimmedLength(s) >= 1);
    }


}
