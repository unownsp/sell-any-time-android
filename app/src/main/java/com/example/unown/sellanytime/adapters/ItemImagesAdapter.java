package com.example.unown.sellanytime.adapters;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.example.unown.sellanytime.R;
import com.example.unown.sellanytime.network.APICreator;
import com.example.unown.sellanytime.pojos.ItemImagesList;

import java.util.ArrayList;

public class ItemImagesAdapter extends RecyclerView.Adapter<ItemImagesAdapter.ItemViewHolder> {

    private ArrayList<ItemImagesList> itemLists = new ArrayList<>();
    private Activity mContext;

    public ItemImagesAdapter( ArrayList<ItemImagesList> itemLists, Activity mContext){

        this.itemLists = itemLists;
        this.mContext = mContext;

    }
    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_item_images, parent, false);
        return new ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {

        ItemImagesList itemImagesList = itemLists.get(position);

        Glide.with(mContext)
                .load(APICreator.ITEM_IMAGE() + itemImagesList.getImage())
                .into(holder.itemImage);

    }

    @Override
    public int getItemCount() {
        return itemLists.size();
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder{

        ImageView itemImage;
        public ItemViewHolder(View itemView) {
            super(itemView);

            itemImage = itemView.findViewById(R.id.item_images);
        }
    }
}
