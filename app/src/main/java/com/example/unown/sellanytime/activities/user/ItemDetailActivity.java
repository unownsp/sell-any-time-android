package com.example.unown.sellanytime.activities.user;

import android.app.ProgressDialog;
import android.arch.persistence.room.Room;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.example.unown.sellanytime.R;
import com.example.unown.sellanytime.adapters.ItemImagesAdapter;
import com.example.unown.sellanytime.database.AppDatabase;
import com.example.unown.sellanytime.database.CartItemPOJO;
import com.example.unown.sellanytime.database.User;
import com.example.unown.sellanytime.network.APICreator;
import com.example.unown.sellanytime.pojos.ItemImagesList;
import com.example.unown.sellanytime.utilities.MySingleton;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ItemDetailActivity extends AppCompatActivity {

    TextView nameTV, categoryTV, priceTV, descriptionTV, addressTV, addedByTV;
    ImageView itemImage;
    Button orderButton, addToCartButton, removeFromCartButton;

    AppDatabase appDatabase;

    RecyclerView recyclerView;
    RecyclerView.LayoutManager layoutManager;
    RecyclerView.Adapter adapter;

    ArrayList<ItemImagesList> list = new ArrayList<>();

    ProgressDialog progressDialog;
    String full_data, name, description, category, price, location, image, access_token, user;
    int item_id, recommendd;
    String TAG = "ItemDetailActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_detail);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        init();

        checkCart();

        listeners();

    }

    private void init() {

        Bundle bundle = getIntent().getExtras();
        full_data = bundle.getString("detail");

        nameTV = findViewById(R.id.itemDetail_nameTV);
        categoryTV = findViewById(R.id.itemDetail_categoryTV);
        priceTV = findViewById(R.id.itemDetail_priceTV);
        descriptionTV = findViewById(R.id.itemDetail_descriptionTV);
        addressTV = findViewById(R.id.itemDetail_addressTV);
        addedByTV = findViewById(R.id.itemDetail_addedByTV);

        itemImage = findViewById(R.id.itemDetail_image);

        orderButton = findViewById(R.id.itemDetail_orderItem);
        addToCartButton = findViewById(R.id.itemDetail_AddToCart);
        removeFromCartButton = findViewById(R.id.itemDetail_RemoveFromCart);

        progressDialog = new ProgressDialog(ItemDetailActivity.this);

        appDatabase = Room.databaseBuilder(getApplicationContext(),
                AppDatabase.class,
                "userdb")
                .fallbackToDestructiveMigration()
                .allowMainThreadQueries()
                .build();
        List<User> user = appDatabase.myDao().getUser();

        for (User usr : user) {
            access_token = usr.getAccess_token();

            Log.d(TAG, "init: " + access_token);

        }
        appDatabase.close();

        setData();
    }

    private void setData() {


        try {
            JSONObject jsonObject = new JSONObject(full_data);

            Log.d(TAG, "setData: -------------------------" + full_data);

            name = jsonObject.getString("name");
            description = jsonObject.getString("description");

            image = jsonObject.getString("image");
            String image1 = jsonObject.getString("image1");
            String image2 = jsonObject.getString("image2");
            String image3 = jsonObject.getString("image3");

            ItemImagesList imagesList = new ItemImagesList();
            imagesList.setImage(image);
            list.add(imagesList);

            if (!image1.equals("default.jpg")) {
                ItemImagesList itemImagesList = new ItemImagesList();
                itemImagesList.setImage(image1);
                list.add(itemImagesList);
            }
            if (!image2.equals("default.jpg")) {
                ItemImagesList itemImagesList = new ItemImagesList();
                itemImagesList.setImage(image2);
                list.add(itemImagesList);
            }
            if (!image3.equals("default.jpg")) {
                ItemImagesList itemImagesList = new ItemImagesList();
                itemImagesList.setImage(image3);
                list.add(itemImagesList);
            }

            recyclerView = findViewById(R.id.itemDetail_itemImagesRecycler);
            recyclerView.setHasFixedSize(true);

            layoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
            recyclerView.setLayoutManager(layoutManager);

            adapter = new ItemImagesAdapter(list, ItemDetailActivity.this);
            recyclerView.setAdapter(adapter);

            price = jsonObject.getString("price");

            recommendd = jsonObject.getInt("isRecommendation");


            if (recommendd == 1) {

                addedByTV.setText("Admin");
            } else {

                user = jsonObject.getString("user");
                JSONObject JO = new JSONObject(user);
                String firstName = JO.getString("first_name");
                String lastName = JO.getString("last_name");
                String fullName = firstName + " " + lastName;

                addedByTV.setText(fullName);

            }
            try {
                location = jsonObject.getString("avaliable_location");

            } catch (Exception e) {
                location = jsonObject.getString("needed_location");

            }

            try {
                category = jsonObject.getString("category");

            } catch (Exception e) {

                category = "---";

            }

            item_id = jsonObject.getInt("id");

            nameTV.setText(name);
            descriptionTV.setText(description);
            priceTV.setText(price);
            addressTV.setText(location);
            categoryTV.setText(category);

            Glide.with(getApplicationContext())
                    .load(APICreator.ITEM_IMAGE() + image)
                    .into(itemImage);

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void checkCart() {

        appDatabase = Room.databaseBuilder(getApplicationContext(),
                AppDatabase.class,
                "userdb")
                .fallbackToDestructiveMigration()
                .allowMainThreadQueries()
                .build();

        List<CartItemPOJO> cartItemPOJOS = appDatabase.cartDao().getCartItem();

        for (CartItemPOJO cartItemPOJO : cartItemPOJOS) {

            int cartItemId = cartItemPOJO.getId();

            if (cartItemId == item_id) {
                addToCartButton.setVisibility(View.GONE);
                removeFromCartButton.setVisibility(View.VISIBLE);

                return;
            }
        }

        addToCartButton.setVisibility(View.VISIBLE);
        removeFromCartButton.setVisibility(View.GONE);

    }

    private void listeners() {

        addedByTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!(recommendd == 1)) {
                    Intent intent = new Intent(getApplicationContext(), AddedByUserDetail.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("user", user);
                    intent.putExtras(bundle);
                    startActivity(intent);
                }
            }
        });

        removeFromCartButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog alertDialog = new AlertDialog.Builder(ItemDetailActivity.this).create();
                alertDialog.setTitle("Remove Form Cart??");
                alertDialog.setMessage("Are you sure you want to remove this item from cart?");
                alertDialog.getWindow().setBackgroundDrawableResource(R.color.white);
                alertDialog.setCanceledOnTouchOutside(false);
                alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "NO",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "YES", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        try {
                            CartItemPOJO toDeleteItem = new CartItemPOJO();
                            toDeleteItem.setId(item_id);

                            appDatabase.cartDao().deleteItem(toDeleteItem);
                            Toast.makeText(getApplicationContext(), "Item Removed From Cart", Toast.LENGTH_SHORT).show();
                            finish();

                        } catch (Exception e) {
                            Toast.makeText(getApplicationContext(), "Unable to Remove Item", Toast.LENGTH_SHORT).show();
                        }


                        if (UserMainActivity.self != null) {
                            UserMainActivity.self.updateCart();
                        }
                    }
                });
                alertDialog.show();
            }
        });

        orderButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog alertDialog = new AlertDialog.Builder(ItemDetailActivity.this).create();
                alertDialog.setTitle("Order The Item?");
                alertDialog.setMessage("Are you sure you want to order this Item??");
                alertDialog.getWindow().setBackgroundDrawableResource(R.color.white);
                alertDialog.setCanceledOnTouchOutside(false);
                alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "NO",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "YES", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        apply();
                    }
                });
                alertDialog.show();
            }
        });

        addToCartButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                AlertDialog alertDialog = new AlertDialog.Builder(ItemDetailActivity.this).create();
                alertDialog.setTitle("Add To Cart??");
                alertDialog.setMessage("Are you sure you want to add this item to your cart?");
                alertDialog.getWindow().setBackgroundDrawableResource(R.color.white);
                alertDialog.setCanceledOnTouchOutside(false);
                alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "NO",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "YES", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        addToCart();
                    }
                });
                alertDialog.show();
            }
        });

    }

    private void addToCart() {

        try {

            appDatabase = Room.databaseBuilder(getApplicationContext(),
                    AppDatabase.class,
                    "userdb")
                    .fallbackToDestructiveMigration()
                    .allowMainThreadQueries()
                    .build();

            CartItemPOJO cartItemPOJO = new CartItemPOJO();
            cartItemPOJO.setAddress(location);
            cartItemPOJO.setCategory(category);
            cartItemPOJO.setDescription(description);
            cartItemPOJO.setId(item_id);
            cartItemPOJO.setImage(image);
            cartItemPOJO.setPrice(price);
            cartItemPOJO.setName(name);
            appDatabase.cartDao().insertItem(cartItemPOJO);

            if (UserMainActivity.self != null) {
                UserMainActivity.self.updateCart();
            }

            Toast.makeText(getApplicationContext(), "Item added to the cart", Toast.LENGTH_SHORT).show();

            finish();

        } catch (Exception e) {

            Toast.makeText(getApplicationContext(), "Unable to add item", Toast.LENGTH_SHORT).show();

        }

    }

    private void apply() {

        progressDialog.setTitle("Requesting");
        progressDialog.setMessage("Please Wait");
        progressDialog.setCancelable(false);

        if (!progressDialog.isShowing()) {
            progressDialog.show();
        }

        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                APICreator.ORDER_ITEM() + item_id,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.d(TAG, "onResponse: " + response);

                        if (progressDialog.isShowing()) {
                            progressDialog.dismiss();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.d(TAG, "onErrorResponse: " + error.toString());

                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

            }
        }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("item_id", item_id + "");
                params.put("_method", "put");
                params.put("task", "reserve");
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> headers = new HashMap<>();
                headers.put("Accept", "application/json");
                headers.put("Authorization", "Bearer " + access_token);
                return headers;
            }
        };

        MySingleton.getInstance(getApplicationContext()).addToRequestque(stringRequest);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
