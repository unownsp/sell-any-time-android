package com.example.unown.sellanytime.activities;


import android.app.ProgressDialog;
import android.arch.persistence.room.Room;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.unown.sellanytime.R;
import com.example.unown.sellanytime.activities.admin.AdminMainActivity;
import com.example.unown.sellanytime.activities.user.ForgotPasswordActivity;
import com.example.unown.sellanytime.activities.user.UserMainActivity;
import com.example.unown.sellanytime.customUI.ShowToast;
import com.example.unown.sellanytime.customUI.UIHelper;
import com.example.unown.sellanytime.database.AppDatabase;
import com.example.unown.sellanytime.database.User;
import com.example.unown.sellanytime.network.APICreator;
import com.example.unown.sellanytime.network.InternetConnectionCheck;
import com.example.unown.sellanytime.utilities.MySingleton;
import com.example.unown.sellanytime.utilities.SharedPref;
import com.example.unown.sellanytime.utilities.Validator;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends AppCompatActivity {

    CardView logintext;
    Animation loginAnim;
    Button loginButton;
    EditText emailET, passwordET;
    TextView forgotpasswordTV;
    ImageView indicatorEmail, indicatorPassword;
    String email, password;
    String TAG = "LoginPage";
    String access_token;

    private ProgressDialog progressDialog;

    public static AppDatabase appDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        init();
        listener();

    }

    private void init() {

        logintext = findViewById(R.id.loginTextCard);
        loginButton = findViewById(R.id.loginButton);

        indicatorEmail = findViewById(R.id.indicatorEmail);
        indicatorPassword = findViewById(R.id.indicatorPass);

        loginAnim = AnimationUtils.loadAnimation(LoginActivity.this, R.anim.card_anim);
        logintext.setAnimation(loginAnim);

        forgotpasswordTV = findViewById(R.id.forgotPassword);

        emailET = findViewById(R.id.emailET);
        passwordET = findViewById(R.id.passwordET);

        progressDialog = new ProgressDialog(LoginActivity.this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait...");

        SharedPref.init(this);

        appDatabase = Room.databaseBuilder(getApplicationContext(),
                AppDatabase.class,
                "userdb")
                .fallbackToDestructiveMigration()
                .allowMainThreadQueries()
                .build();

        emailET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (Validator.isValidEmail(charSequence)) {
                    UIHelper.validationSucessTint(LoginActivity.this, indicatorEmail);
                } else {
                    UIHelper.validationNormalTint(LoginActivity.this, indicatorEmail);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        passwordET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                if (Validator.isValidPassword(charSequence)) {
                    UIHelper.validationSucessTint(LoginActivity.this, indicatorPassword);
                } else {
                    UIHelper.validationNormalTint(LoginActivity.this, indicatorPassword);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });


    }

    private void listener() {

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (valid()) {
                    login();
                }
            }
        });

        forgotpasswordTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(),ForgotPasswordActivity.class));
            }
        });

    }

    public boolean valid() {

        email = emailET.getText().toString();
        password = passwordET.getText().toString();

        boolean validEmail = Validator.isValidEmail(email);
        boolean validPassword = Validator.isValidPassword(password);

        if (!validEmail) {
            UIHelper.validationFailedTint(LoginActivity.this, indicatorEmail);
            UIHelper.validationFailedEditText(LoginActivity.this, emailET);
            customToast("Invalid Email");
            return false;
        } else if (!validPassword) {
            UIHelper.validationFailedTint(LoginActivity.this, indicatorPassword);
            UIHelper.validationFailedEditText(LoginActivity.this, passwordET);
            customToast("Invalid Password");
            return false;
        } else {
            return true;
        }

    }

    public void customToast(String msg){
        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.custom_toast,(ViewGroup) findViewById(R.id.toast_root));

        TextView text = layout.findViewById(R.id.toast_error);
        text.setText(msg);

        Toast toast = new Toast(getApplicationContext());
        toast.setGravity(Gravity.TOP | Gravity.FILL_HORIZONTAL, 0, 0);// Set
        toast.setDuration(Toast.LENGTH_SHORT);// Set Duration
        toast.setView(layout); // Set Custom View over toast
        toast.show();
    }

    public void login() {

        progressDialog.setTitle("Logging in");
        progressDialog.show();

        boolean internetAvailable = InternetConnectionCheck.isInternetAvailable(getApplicationContext());
        if (!internetAvailable){

            AlertDialog alertDialog = new AlertDialog.Builder(LoginActivity.this).create();
            alertDialog.getWindow().setBackgroundDrawableResource(R.color.white);
            alertDialog.setCanceledOnTouchOutside(false);
            alertDialog.setTitle(Html.fromHtml("Unable to connect"));
            alertDialog.setMessage(Html.fromHtml("Please check your internet connection."));
            alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, Html.fromHtml("ok"),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
            alertDialog.show();

            return;
        }

        StringRequest stringRequest = new StringRequest(Request.Method.POST, APICreator.TOKEN(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.d(TAG, "onResponse: " + response);

                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            String token_type = jsonObject.getString("token_type");
                            String expires_in = jsonObject.getString("expires_in");
                            access_token = jsonObject.getString("access_token");
                            String refresh_token = jsonObject.getString("refresh_token");

                            try {
                                User user = new User();
                                user.setId(1);
                                user.setAccess_token(access_token);
                                user.setExpires_in(expires_in);
                                user.setRemember_token(refresh_token);
                                user.setToken_type(token_type);

                                appDatabase.myDao().insertDetail(user);
                                Log.d(TAG, "onResponse: " + "Data Added successfully!!!--------->");

                            } catch (Exception e) {
                                User user = new User();
                                user.setId(1);

                                appDatabase.myDao().deleteUser(user);

                                Log.d(TAG, "onResponse: " + "Data deleted successfully!!!------->");

                                user.setAccess_token(access_token);
                                user.setExpires_in(expires_in);
                                user.setRemember_token(refresh_token);
                                user.setToken_type(token_type);

                                appDatabase.myDao().insertDetail(user);

                                Log.d(TAG, "onResponse: " + "Data Added successfully!!!--------->");
                            }

                            if (progressDialog.isShowing()) {
                                progressDialog.dismiss();
                            }
//--------------------------------------------------------------------------------------------------

                            getUserDetail();

//--------------------------------------------------------------------------------------------------

                            if (progressDialog.isShowing()) {
                                progressDialog.dismiss();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "onErrorResponse: " + error);

                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null && networkResponse.statusCode == 401){

                    AlertDialog alertDialog = new AlertDialog.Builder(LoginActivity.this).create();
                    alertDialog.getWindow().setBackgroundDrawableResource(R.color.white);
                    alertDialog.setCanceledOnTouchOutside(false);
                    alertDialog.setTitle(Html.fromHtml("Unable to login"));
                    alertDialog.setMessage(Html.fromHtml("Username/password did not match!"));
                    alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, Html.fromHtml("ok"),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                    alertDialog.show();

                }else if (networkResponse != null && networkResponse.statusCode == 504){

                    AlertDialog alertDialog = new AlertDialog.Builder(LoginActivity.this).create();
                    alertDialog.getWindow().setBackgroundDrawableResource(R.color.white);
                    alertDialog.setCanceledOnTouchOutside(false);
                    alertDialog.setTitle(Html.fromHtml("Unable to login"));
                    alertDialog.setMessage(Html.fromHtml("Username/password did not match!"));
                    alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, Html.fromHtml("ok"),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                    alertDialog.show();

                }

                if (error.toString().equals("com.android.volley.TimeoutError")) {
                    ShowToast.showErrorMessage(getApplicationContext(), "Server took too long to respond");

                    AlertDialog alertDialog = new AlertDialog.Builder(LoginActivity.this).create();
                    alertDialog.getWindow().setBackgroundDrawableResource(R.color.white);
                    alertDialog.setCanceledOnTouchOutside(false);
                    alertDialog.setTitle(Html.fromHtml("Error"));
                    alertDialog.setMessage(Html.fromHtml("Server Did not respond"));
                    alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, Html.fromHtml("ok"),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                    alertDialog.show();
                }

            }
        }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<>();
                params.put("grant_type", "password");
                params.put("client_id", "3");
                params.put("client_secret", "uCucWzqCLOgbdw8hO45cvR39oFvmnYD1Hg9rDrZ7");
                params.put("scope", "*");

                params.put("username", email);
                params.put("password", password);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> headers = new HashMap<>();
                headers.put("Accept", "application/json");
                return headers;
            }
        };

        int timeout = 30000;
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(timeout,DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MySingleton.getInstance(getApplicationContext()).addToRequestque(stringRequest);

    }

    public void getUserDetail() {


        progressDialog.setTitle("Loading your information");
        progressDialog.show();

        StringRequest getDetail = new StringRequest(Request.Method.GET, APICreator.CURRENTUSER(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.d(TAG, "onResponse: user Detail" + response);

                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            int id = jsonObject.getInt("id");
                            int role = jsonObject.getInt("role_id");
                            String firstName = jsonObject.getString("first_name");
                            String lastName = jsonObject.getString("last_name");
                            String phoneNumber = jsonObject.getString("phoneNumber");
                            String address = jsonObject.getString("address");
                            String image = jsonObject.getString("image");
                            String email = jsonObject.getString("email");
                            String wallet = jsonObject.getString("wallet");

                            SharedPref.write(SharedPref.FIRST_NAME,firstName);
                            SharedPref.write(SharedPref.LAST_NAME,lastName);
                            SharedPref.write(SharedPref.PHONENUMBER ,phoneNumber);
                            SharedPref.write(SharedPref.ADDRESS,address);
                            SharedPref.write(SharedPref.IMAGE,image);
                            SharedPref.write(SharedPref.EMAIL,email);
                            SharedPref.write(SharedPref.ROLE_ID,role);
                            SharedPref.write(SharedPref.UID,id+"");
                            SharedPref.write(SharedPref.WALLET,wallet+"");

                            SharedPref.write(SharedPref.IS_LOGGED_IN,true);

                            if (role == 1) {


                                SharedPref.write(SharedPref.IS_USER,true);
                                startActivity(new Intent(LoginActivity.this, UserMainActivity.class));

                            } else if (role == 2) {

                                SharedPref.write(SharedPref.IS_USER,false);
                                startActivity(new Intent(LoginActivity.this, AdminMainActivity.class));

                            } else {

                                ShowToast.showErrorMessage(getApplicationContext(), "Data Error");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        if (progressDialog.isShowing()) {
                            progressDialog.dismiss();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "onErrorResponse: user detail error " + error);

                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                if (error.toString().equals("com.android.volley.TimeoutError")) {
                    ShowToast.showErrorMessage(getApplicationContext(), "Server took too long to respond");

                    AlertDialog alertDialog = new AlertDialog.Builder(LoginActivity.this).create();
                    alertDialog.getWindow().setBackgroundDrawableResource(R.color.white);
                    alertDialog.setCanceledOnTouchOutside(false);
                    alertDialog.setTitle(Html.fromHtml("Error"));
                    alertDialog.setMessage(Html.fromHtml("Server Did not respond"));
                    alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, Html.fromHtml("ok"),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                    alertDialog.show();
                }
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("Accept", "application/json");
                params.put("Authorization", "Bearer " + access_token);
                return params;
            }
        };

        MySingleton.getInstance(getApplicationContext()).addToRequestque(getDetail);

    }
}