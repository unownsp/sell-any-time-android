package com.example.unown.sellanytime.adapters;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.unown.sellanytime.R;
import com.example.unown.sellanytime.activities.user.RequestedItemDetailActivity;
import com.example.unown.sellanytime.network.APICreator;
import com.example.unown.sellanytime.pojos.RequestedItemList;

import java.util.ArrayList;

public class RequestedItemAdapter extends RecyclerView.Adapter<RequestedItemAdapter.ItemViewHolder> {

    ArrayList<RequestedItemList> items = new ArrayList<>();
    private Activity mContext;

    public RequestedItemAdapter(ArrayList<RequestedItemList> items, Activity mContext) {
        this.items = items;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_item_main, parent, false);
        ItemViewHolder itemViewHolder = new ItemViewHolder(view);
        return itemViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {

        final RequestedItemList requestedItemList =items.get(position);
        holder.location.setText(requestedItemList.getLocation());
        holder.title.setText(requestedItemList.getTitle());
        holder.price.setText(requestedItemList.getPrice());

        Glide.with(mContext)
                .load(APICreator.ITEM_IMAGE() + requestedItemList.getImage())
                .into(holder.item_image);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(mContext, RequestedItemDetailActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("detail", requestedItemList.getFull_detail());
                intent.putExtras(bundle);
                mContext.startActivity(intent);

                Toast.makeText(mContext,"nice",Toast.LENGTH_SHORT).show();

            }
        });
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {

        ImageView item_image;
        TextView title,price,location;
        public ItemViewHolder(View itemView) {
            super(itemView);

            title = itemView.findViewById(R.id.item_itemName);
            price = itemView.findViewById(R.id.item_itemPrice);
            location = itemView.findViewById(R.id.item_itemLocation);

            item_image = itemView.findViewById(R.id.item_itemImage);
        }
    }
}
