package com.example.unown.sellanytime.activities.user;

import android.app.ProgressDialog;
import android.arch.persistence.room.Room;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.unown.sellanytime.R;
import com.example.unown.sellanytime.database.AppDatabase;
import com.example.unown.sellanytime.database.User;
import com.example.unown.sellanytime.network.APICreator;
import com.example.unown.sellanytime.utilities.ImageEncoder;
import com.example.unown.sellanytime.utilities.MySingleton;
import com.example.unown.sellanytime.utilities.SharedPref;
import com.myhexaville.smartimagepicker.ImagePicker;
import com.myhexaville.smartimagepicker.OnImagePickedListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AddItemActivity extends AppCompatActivity {

    EditText name_et, price_et, address_et, description_et;
    Button addItemBtn, addItemImageBtn1, addItemImageBtn2, addItemImageBtn3, addMoreImage, cancelImages;
    ImageView itemImage, cameraIcon, itemImage1, itemImage2, itemImage3;

    LinearLayout moreImageLayout;
    ImagePicker imagePicker;
    String access_token, name, price, category="", address, description, TAG = "AddItemActivity";
    AppDatabase appDatabase;
    boolean isImage = false, isImage1 = false, isImage2 = false, isImage3 = false;
    String encodedImage, encodedImage1 = "", encodedImage2 = "", encodedImage3 = "";

    Spinner categorySpinner;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_item);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        init();
        listeners();
    }

    private void init() {
        name_et = findViewById(R.id.addItem_nameET);
        price_et = findViewById(R.id.addItem_priceET);
        //category_et = findViewById(R.id.addItem_categoryET);
        address_et = findViewById(R.id.addItem_addressET);
        description_et = findViewById(R.id.addItem_descriptionET);

        categorySpinner = findViewById(R.id.categorySpinner);
        String categories[] = {"Electronics", "Day to Day", "Sporting goods", "Babies and Toys", "Health and Beauty", "Watches and Accessories", "Home Appliances"};

        ArrayAdapter<String> item_category = new ArrayAdapter<>(getApplicationContext(), R.layout.row_spinner_item, categories);
        item_category.setDropDownViewResource(R.layout.row_spinner_dropdownitem_list);
        categorySpinner.setAdapter(item_category);

        categorySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                category = categorySpinner.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        SharedPref.init(getApplicationContext());

        progressDialog = new ProgressDialog(AddItemActivity.this);
        progressDialog.setTitle("Adding Items");
        progressDialog.setMessage("Please wait");

        addItemBtn = findViewById(R.id.addItem_AddItemBtn);

        addItemImageBtn1 = findViewById(R.id.addItem_addImageButton1);
        addItemImageBtn2 = findViewById(R.id.addItem_addImageButton2);
        addItemImageBtn3 = findViewById(R.id.addItem_addImageButton3);

        addMoreImage = findViewById(R.id.addItem_addMoreImageButton);
        cancelImages = findViewById(R.id.addItem_cancelImagesButton);

        moreImageLayout = findViewById(R.id.addItem_moreImageLayout);

        itemImage = findViewById(R.id.addItem_ItemImage);
        itemImage1 = findViewById(R.id.addItem_itemImage1);
        itemImage2 = findViewById(R.id.addItem_itemImage2);
        itemImage3 = findViewById(R.id.addItem_itemImage3);

        cameraIcon = findViewById(R.id.addItem_cameraIcon);

        appDatabase = Room.databaseBuilder(getApplicationContext(),
                AppDatabase.class,
                "userdb")
                .fallbackToDestructiveMigration()
                .allowMainThreadQueries()
                .build();
        List<User> user = appDatabase.myDao().getUser();

        for (User usr : user) {
            access_token = usr.getAccess_token();

        }

        try {

            imagePicker = new ImagePicker(AddItemActivity.this, null, new OnImagePickedListener() {
                @Override
                public void onImagePicked(Uri imageUri) {

                    System.out.println("imageUri-->" + imageUri);
                    final InputStream imageStream;
                    try {
                        imageStream = getApplicationContext().getContentResolver().openInputStream(imageUri);
                        final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
                        if (isImage) {
                            encodedImage = ImageEncoder.encodeImage(selectedImage);
                            itemImage.setImageURI(imageUri);
                            System.out.println("------------------------->" + encodedImage);
                        } else if (isImage1) {
                            encodedImage1 = ImageEncoder.encodeImage(selectedImage);
                            itemImage1.setImageURI(imageUri);
                            System.out.println("------------------------->" + encodedImage1);
                        } else if (isImage2) {
                            encodedImage2 = ImageEncoder.encodeImage(selectedImage);
                            itemImage2.setImageURI(imageUri);
                            System.out.println("------------------------->" + encodedImage2);
                        } else if (isImage3) {
                            encodedImage3 = ImageEncoder.encodeImage(selectedImage);
                            itemImage3.setImageURI(imageUri);
                            System.out.println("------------------------->" + encodedImage3);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            }).setWithImageCrop(1, 1);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void listeners() {

        addMoreImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                moreImageLayout.setVisibility(View.VISIBLE);
            }
        });

        addItemImageBtn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isImage = false;
                isImage1 = true;
                isImage2 = false;
                isImage3 = false;
                imagePicker.choosePicture(true);
            }
        });

        addItemImageBtn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isImage = false;
                isImage1 = false;
                isImage2 = true;
                isImage3 = false;
                imagePicker.choosePicture(true);
            }
        });

        addItemImageBtn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isImage = false;
                isImage1 = false;
                isImage2 = false;
                isImage3 = true;
                imagePicker.choosePicture(true);
            }
        });

        cancelImages.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                moreImageLayout.setVisibility(View.GONE);
                encodedImage1 = "";
                encodedImage2 = "";
                encodedImage3 = "";
            }
        });


        cameraIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isImage = true;
                isImage1 = false;
                isImage2 = false;
                isImage3 = false;
                imagePicker.choosePicture(true);
            }
        });

        addItemBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (valid()) {

                    if (!progressDialog.isShowing()) {
                        progressDialog.show();
                    }

                    StringRequest stringRequest = new StringRequest(Request.Method.POST, APICreator.ADD_ITEM(),
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {

                                    Log.d(TAG, "onResponse: " + response);

                                    try {
                                        JSONObject jsonObject = new JSONObject(response);

                                        String status = jsonObject.getString("status");

                                        if (status.equals("success")) {

                                            if (progressDialog.isShowing()) {
                                                progressDialog.dismiss();
                                            }
                                            Toast.makeText(getApplicationContext(), "Item Added", Toast.LENGTH_SHORT).show();
                                            finish();
                                        } else {

                                            if (progressDialog.isShowing()) {
                                                progressDialog.dismiss();
                                            }
                                            Toast.makeText(getApplicationContext(), "Unable to add Item", Toast.LENGTH_SHORT).show();

                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                }
                            }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                            Log.d(TAG, "onErrorResponse: " + error);

                            if (progressDialog.isShowing()) {
                                progressDialog.dismiss();
                            }

                        }
                    }) {


                        @Override
                        public Map<String, String> getHeaders() throws AuthFailureError {
                            Map<String, String> params = new HashMap<>();
                            params.put("Accept", "application/json");
                            params.put("Authorization", "Bearer " + access_token);
                            return params;
                        }

                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {
                            Map<String, String> params = new HashMap<>();
                            params.put("name", name);
                            params.put("description", description);
                            params.put("price", price);
                            params.put("image", encodedImage);
                            params.put("image1", encodedImage1);
                            params.put("image2", encodedImage2);
                            params.put("image3", encodedImage3);
                            params.put("avaliable_location", address);
                            params.put("user_id", SharedPref.read(SharedPref.UID, null));
                            params.put("category", category);
                            return params;
                        }
                    };

                    MySingleton.getInstance(getApplicationContext()).addToRequestque(stringRequest);

                }
            }
        });
    }

    private boolean valid() {

        name = name_et.getText().toString();
        price = price_et.getText().toString();
        //category = category_et.getText().toString();
        description = description_et.getText().toString();
        address = address_et.getText().toString();

        System.out.println(name + "        " + price + "        " + category + "id = " + SharedPref.read(SharedPref.UID, null) + "        " + description + address);


        return true;
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        imagePicker.handlePermission(requestCode, grantResults);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        imagePicker.handleActivityResult(resultCode, requestCode, data);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
