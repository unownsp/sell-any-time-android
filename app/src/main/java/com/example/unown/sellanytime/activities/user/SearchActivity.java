package com.example.unown.sellanytime.activities.user;

import android.arch.persistence.room.Room;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.unown.sellanytime.R;
import com.example.unown.sellanytime.adapters.ItemListAdapter;
import com.example.unown.sellanytime.database.AppDatabase;
import com.example.unown.sellanytime.database.User;
import com.example.unown.sellanytime.network.APICreator;
import com.example.unown.sellanytime.pojos.ItemListMain;
import com.example.unown.sellanytime.utilities.MySingleton;
import com.miguelcatalan.materialsearchview.MaterialSearchView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SearchActivity extends AppCompatActivity {

    MaterialSearchView searchView;

    TextView searchText;

    RecyclerView recyclerView;
    RecyclerView.LayoutManager layoutManager;
    RecyclerView.Adapter adapter;

    ArrayList<ItemListMain> list = new ArrayList<>();
    ArrayList<ItemListMain> newList = new ArrayList<>();

    AppDatabase appDatabase;

    String TAG = "SearchActivity";
    String access_token;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        searchText = findViewById(R.id.searchText);

        appDatabase = Room.databaseBuilder(getApplicationContext(),
                AppDatabase.class,
                "userdb")
                .fallbackToDestructiveMigration()
                .allowMainThreadQueries()
                .build();
        List<User> user = appDatabase.myDao().getUser();

        for (User usr : user) {
            access_token = usr.getAccess_token();

        }

        recyclerView = findViewById(R.id.searchRecycler);
        recyclerView.setHasFixedSize(true);

        getItems();
    }

    public void getItems() {

        StringRequest stringRequest = new StringRequest(Request.Method.GET, APICreator.ITEMS(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.d(TAG, "onResponse: " + response);
                        list.clear();

                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = jsonObject.getJSONArray("data");

                            for (int i = 0; i < jsonArray.length(); i++) {

                                JSONObject JO = jsonArray.getJSONObject(i);

                                Log.d(TAG, "onResponse: " + JO.toString());

                                String full_detail = JO.toString();

                                String name = JO.getString("name");
                                int id = JO.getInt("id");
                                String description = JO.getString("description");
                                String image = JO.getString("image");
                                String avaliable_location = JO.getString("avaliable_location");
                                String category = JO.getString("category");
                                String price = JO.getString("price");
                                String user = JO.getString("user");

                                JSONObject user_detail = new JSONObject(user);
                                String user_name = user_detail.getString("first_name");
                                String user_email = user_detail.getString("email");
                                String user_image = user_detail.getString("image");


                                ItemListMain itemListMain = new ItemListMain(name, price, avaliable_location, description, image, category, user, user_name, user_email, user_image, id, full_detail);
                                list.add(itemListMain);
                            }

                            layoutManager = new LinearLayoutManager(getApplicationContext());
                            recyclerView.setLayoutManager(layoutManager);

                            adapter = new ItemListAdapter(list, SearchActivity.this);
                            recyclerView.setAdapter(adapter);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.d(TAG, "onResponse: " + error);

            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Accept", "application/json");
                headers.put("Authorization", "Bearer " + access_token);

                return headers;
            }
        };

        MySingleton.getInstance(getApplicationContext()).addToRequestque(stringRequest);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.user_search, menu);

        searchView = findViewById(R.id.search_view);
        MenuItem item = menu.findItem(R.id.action_search);
        searchView.setMenuItem(item);

        searchView.setOnSearchViewListener(new MaterialSearchView.SearchViewListener() {
            @Override
            public void onSearchViewShown() {

            }

            @Override
            public void onSearchViewClosed() {

            }
        });

        searchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                Toast.makeText(getApplicationContext(),"submit",Toast.LENGTH_SHORT).show();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                if (newText.length()>2){

                    searchText.setText(newText);
                    newList.clear();

                    for (ItemListMain itemListMain: list) {

                        String saved = itemListMain.getTitle().toLowerCase();
                        String typed = newText.toLowerCase();

                        if (saved.contains(typed)){

                            String full_detail = itemListMain.getFull_detail();

                            String name = itemListMain.getTitle();
                            int id = itemListMain.getId();
                            String description = itemListMain.getDescription();
                            String image = itemListMain.getImage();
                            String avaliable_location = itemListMain.getLocation();
                            String category = itemListMain.getCategory();
                            String price = itemListMain.getPrice();
                            String user = itemListMain.getUser();

                            String user_name = "first_name";
                            String user_email ="email";
                            String user_image = "image";


                            ItemListMain newItemListMain = new ItemListMain(name, price, avaliable_location, description, image, category, user, user_name, user_email, user_image, id, full_detail);
                            newList.add(newItemListMain);

                        }

                    }

                    adapter = new ItemListAdapter(newList, SearchActivity.this);
                    recyclerView.setAdapter(adapter);
                    adapter.notifyDataSetChanged();

                }

                if (newText.isEmpty()){

                    searchText.setText(" --- ");
                    adapter = new ItemListAdapter(list, SearchActivity.this);
                    recyclerView.setAdapter(adapter);
                    adapter.notifyDataSetChanged();

                }
                return false;
            }
        });


        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
