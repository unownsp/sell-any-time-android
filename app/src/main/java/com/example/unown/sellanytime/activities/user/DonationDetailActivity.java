package com.example.unown.sellanytime.activities.user;

import android.app.ProgressDialog;
import android.arch.persistence.room.Room;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.example.unown.sellanytime.R;
import com.example.unown.sellanytime.database.AppDatabase;
import com.example.unown.sellanytime.database.User;
import com.example.unown.sellanytime.network.APICreator;
import com.example.unown.sellanytime.utilities.MySingleton;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DonationDetailActivity extends AppCompatActivity {

    TextView nameTV, descriptionTV, addressTV;
    ImageView itemImage;
    Button orderButton;

    AppDatabase appDatabase;

    ProgressDialog progressDialog;
    String full_data, name, description, price, location, image, access_token;
    int item_id;
    String TAG = "DonationDetailActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_donation_detail);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        init();

        listeners();
    }

    private void init() {

        Bundle bundle = getIntent().getExtras();
        full_data = bundle.getString("detail");

        nameTV = findViewById(R.id.donationDetail_nameTV);
        descriptionTV = findViewById(R.id.donationDetail_descriptionTV);
        addressTV = findViewById(R.id.donationDetail_addressTV);

        itemImage = findViewById(R.id.donationDetail_image);

        orderButton = findViewById(R.id.donationDetail_orderItem);

        progressDialog = new ProgressDialog(DonationDetailActivity.this);

        appDatabase = Room.databaseBuilder(getApplicationContext(),
                AppDatabase.class,
                "userdb")
                .fallbackToDestructiveMigration()
                .allowMainThreadQueries()
                .build();
        List<User> user = appDatabase.myDao().getUser();

        for (User usr : user) {
            access_token = usr.getAccess_token();

            Log.d(TAG, "init: " + access_token);

        }
        appDatabase.close();

        setData();
    }

    private void setData() {


        try {
            JSONObject jsonObject = new JSONObject(full_data);

            name = jsonObject.getString("name");
            description = jsonObject.getString("description");
            image = jsonObject.getString("image");
            price = jsonObject.getString("price");
            location = jsonObject.getString("needed_location");

            item_id = jsonObject.getInt("id");

            nameTV.setText(name);
            descriptionTV.setText(description);
            addressTV.setText(location);

            Glide.with(getApplicationContext())
                    .load(APICreator.ITEM_IMAGE() + image)
                    .into(itemImage);

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void listeners() {

        orderButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog alertDialog = new AlertDialog.Builder(DonationDetailActivity.this).create();
                alertDialog.setTitle("Order The Item?");
                alertDialog.setMessage("Are you sure you want to order this Item??");
                alertDialog.getWindow().setBackgroundDrawableResource(R.color.white);
                alertDialog.setCanceledOnTouchOutside(false);
                alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "NO",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "YES", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        apply();
                    }
                });
                alertDialog.show();


            }
        });

    }

    private void apply(){

        progressDialog.setTitle("Requesting");
        progressDialog.setMessage("Please Wait");
        progressDialog.setCancelable(false);

        if (!progressDialog.isShowing()) {
            progressDialog.show();
        }

        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                APICreator.ORDER_DONATION()+item_id,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.d(TAG, "onResponse: "+response);

                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String status = jsonObject.getString("status");

                            if (status.equals("success")){

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        if (progressDialog.isShowing()) {
                            progressDialog.dismiss();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.d(TAG, "onErrorResponse: "+error.toString());

                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

            }
        }){

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("donation_id",item_id+"");
                params.put("_method","put");
                params.put("task","reserve");
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> headers = new HashMap<>();
                headers.put("Accept", "application/json");
                headers.put("Authorization","Bearer "+access_token);
                return headers;
            }
        };

        MySingleton.getInstance(getApplicationContext()).addToRequestque(stringRequest);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
