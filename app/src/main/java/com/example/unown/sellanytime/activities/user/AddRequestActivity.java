package com.example.unown.sellanytime.activities.user;

import android.arch.persistence.room.Room;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.unown.sellanytime.R;
import com.example.unown.sellanytime.database.AppDatabase;
import com.example.unown.sellanytime.database.User;
import com.example.unown.sellanytime.network.APICreator;
import com.example.unown.sellanytime.utilities.MySingleton;
import com.example.unown.sellanytime.utilities.SharedPref;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AddRequestActivity extends AppCompatActivity {

    EditText name_et, price_et, address_et, description_et;
    Button requestBtn;
    Spinner categorySpinner;
    private String access_token,name,price,category,address,description,TAG="AddRequestActivity";
    private AppDatabase appDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_request);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        init();

        listeners();
    }

    private void init(){
        name_et = findViewById(R.id.addRequest_nameET);
        price_et = findViewById(R.id.addRequest_priceET);
        address_et  = findViewById(R.id.addRequest_addressET);
        description_et = findViewById(R.id.addRequest_descriptionET);

        categorySpinner = findViewById(R.id.categorySpinner);
        String categories[] = {"Electronics", "Day to Day", "Sporting goods", "Babies and Toys", "Health and Beauty", "Watches and Accessories", "Home Appliances"};

        ArrayAdapter<String> item_category = new ArrayAdapter<>(getApplicationContext(), R.layout.row_spinner_item, categories);
        item_category.setDropDownViewResource(R.layout.row_spinner_dropdownitem_list);
        categorySpinner.setAdapter(item_category);

        categorySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                category = categorySpinner.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        requestBtn = findViewById(R.id.addRequest_AddItemBtn);

        SharedPref.init(getApplicationContext());

        appDatabase = Room.databaseBuilder(getApplicationContext(),
                AppDatabase.class,
                "userdb")
                .fallbackToDestructiveMigration()
                .allowMainThreadQueries()
                .build();
        List<User> user = appDatabase.myDao().getUser();

        for (User usr : user) {
            access_token = usr.getAccess_token();

        }
    }

    private void listeners(){
        requestBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (valid()){
                    addRequest();
                }

            }
        });
    }

    private boolean valid(){

        name = name_et.getText().toString();
        price = price_et.getText().toString();
        //category = category_et.getText().toString();
        description = description_et.getText().toString();
        address = address_et.getText().toString();

        return true;

    }

    public void customToastError(String msg){
        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.custom_toast,(ViewGroup) findViewById(R.id.toast_root));

        TextView text = layout.findViewById(R.id.toast_error);
        text.setText(msg);

        Toast toast = new Toast(getApplicationContext());
        toast.setGravity(Gravity.TOP | Gravity.FILL_HORIZONTAL, 0, 0);// Set
        toast.setDuration(Toast.LENGTH_SHORT);// Set Duration
        toast.setView(layout); // Set Custom View over toast
        toast.show();
    }
    public void customToastSuccess(String msg){
        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.custom_toast_success,(ViewGroup) findViewById(R.id.toast_root_success));

        TextView text = layout.findViewById(R.id.toast_success);
        text.setText(msg);

        Toast toast = new Toast(getApplicationContext());
        toast.setGravity(Gravity.TOP | Gravity.FILL_HORIZONTAL, 0, 0);// Set
        toast.setDuration(Toast.LENGTH_SHORT);// Set Duration
        toast.setView(layout); // Set Custom View over toast
        toast.show();
    }

    private void addRequest(){

        StringRequest stringRequest = new StringRequest(Request.Method.POST, APICreator.REQUESTS(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.d(TAG, "onResponse: "+response);

                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String status = jsonObject.getString("status");

                            if (status.equals("success")){
                                customToastSuccess("Item Requested");
                                finish();
                            }else{
                                customToastError("Unable to Request Item");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.d(TAG, "onErrorResponse: "+error.toString());

            }
        }){

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("Accept", "application/json");
                params.put("Authorization", "Bearer " + access_token);
                return params;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("name", name);
                params.put("description", description);
                params.put("price", price);
                params.put("needed_location", address);
                params.put("user_id", SharedPref.read(SharedPref.UID,null));
                params.put("category", category);
                return params;
            }

        };

        MySingleton.getInstance(getApplicationContext()).addToRequestque(stringRequest);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
