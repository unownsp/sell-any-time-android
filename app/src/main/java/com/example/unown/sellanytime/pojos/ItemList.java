package com.example.unown.sellanytime.pojos;

public class ItemList {
    private String title;
    private String category;
    private String price;
    private String description;
    private String location;
    private String image;

    public String getFull_detail() {
        return full_detail;
    }

    public void setFull_detail(String full_detail) {
        this.full_detail = full_detail;
    }

    private String full_detail;

    public ItemList(String title, String category, String price, String description, String location, String image,String full_detail) {
        this.title = title;
        this.category = category;
        this.price = price;
        this.description = description;
        this.location = location;
        this.image = image;
        this.full_detail = full_detail;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
